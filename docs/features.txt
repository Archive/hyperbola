				Hyperbola Features

. Searching
	Indexing program
		Filters for HTML, ASCII, man, and info.
		Programs for database creation, maintainance, merging, etc.

	Retrieval
		Routine to return a list of file ID's that contain a specified word.

		Routine to return the title and help path for a file ID.

	Query evaluation
		Basic keyword stuff
		Boolean expressions

. Navigation
	Frequency
		Per-app & global

		btree lib could store this info, but no way to get top 5.
	History
		Need a global history mechanism anyways - redo gnome-history into a CORBA server, perhaps.

. Display
	Tree view
	Data view
		libwww integration

		man2html and info2html converters
