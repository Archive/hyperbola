/*
 * Copyright (C) 1988, Marcus J. Ranum, William Welch Medical Library
 * $Author$
 */

/*
 * $Header$: recio.h
 *
 * $Log$
 * Revision 1.1  1999/11/12 15:47:05  sopwith
 * Initial revision
 *
 * Revision 1.1  88/06/01  21:28:06  mjr
 * Initial revision
 */

#ifndef	_INCL_RECIO_H

#include <glib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

typedef struct rifd *RIFD;
typedef gint32 RIREC;

RIFD riopen(const char *path, int flags, mode_t mode, int reclen);
int riclose(RIFD rf);
int riwrite(RIFD rf, RIREC recno, const void *buf, size_t count, gboolean append);
int riread(RIFD rf, RIREC recno, void *buf, size_t count, gboolean cont);
int riunlink(RIFD rf, RIREC recno);
char *rigets(RIFD rf, RIREC recno, char *buf, size_t buf_size);
int riputs(RIFD rf, RIREC recno, const char *buf);
int risetlab(RIFD rf, const void *buf, size_t count);
int rigetlab(RIFD rf, void *buf, size_t count);

#define	_INCL_RECIO_H
#endif
