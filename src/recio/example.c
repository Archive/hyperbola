/*
 * Copyright (C) 1988, Marcus J. Ranum, William Welch Medical Library
 * $Author$
 */

#ifndef lint
static char *RCSid = "$Header$: example.c";
#endif

/*
 * $Log$
 * Revision 1.1  1999/11/12 15:47:05  sopwith
 * Initial revision
 *
 * Revision 1.1  88/06/01  21:27:06  mjr
 * Initial revision
 *
 */

/* sample page size to use - obviously 20 is quite small, but it makes */
/* testing things really a lot easier than having to enter 1024 bytes of */
/* junk data for testing input */

#define SAMPLESIZE 20


/* nobody ever writes nice test programs. in fact, this one is pretty gross */
/* basically, this allows exercising all the various functions of the rifd */
/* library */

#ifdef BSD
#include <sys/file.h>
#endif
#ifdef SYSV
#include <sys/fcntl.h>
#endif
#include <stdio.h>
#include "recio.h"

void
help()
{
    printf("commands are:\n\
Read:	reads a chunk of the requested record.\n\
	Note that this is artificially constrained in this\n\
	example, to show how the consecutive reads work\n\
More	prints the next chunk in consecutive reads.\n\
Gets	returns pointer to next line.\n");
    printf("Puts appends a newline terminated string to rec.\n\
Del deletes the named record by placing its blocks\n\
    on the free block list \ n \
Label sets the database comment block that resides\n\
    in page #0 of the page file.\n\
Write	allows input of text to a specified record.\n\
Appen	allows appending text to a specified record.\n\
Help	this.\n\
");
}

main(ac, av)
    int     ac;
    char   *av[];
{
    RIFD   h1;
    char    instr[BUFSIZ];
    char    buf[BUFSIZ];
    char   *files;
    long    rec;
    int     ret;
    int     size = SAMPLESIZE;	/* page block size - we use a small */

    /* size to give things a real workout */
    int     num;
    extern long atol();
    extern char *gets();
    extern char *strcat();

    if (ac < 3)
    {
	(void) fprintf(stderr, "(using foo.*)\n");
	files = "foo";
    }
    else
    {
	/* there are keener ways of doing this, but why ? */
	files = av[1];
    }

    (void) printf("record page size to use:(<return> = %d)", size);
    (void) gets(instr);
    if (strlen(instr) != 0)
	size = atoi(instr);

    /* args passed in order: textfile,mapfile */
    if ((h1 = riopen(files, O_CREAT | O_RDWR, 0600, size)) == NULL)
    {
	perror(av[1]);
	exit(1);
    }


    while (1)
    {
	(void) printf("Read  More  Gets  Puts  Del  Label  Write  Appen  Help  Quit:");
	if (gets(instr) == NULL)
	    exit(riclose(h1));

	switch (*instr)
	{

	case 'q':
	case 'Q':
	    exit(riclose(h1));

	case 'd':
	case 'D':
	    (void) printf("delete record:");
	    (void) gets(instr);
	    rec = atol(instr);
	    ret = riunlink(h1, rec);
	    (void) printf("...returns %d\n", ret);
	    break;

	case 'g':
	case 'G':
	    (void) printf("gets from record:(<return> = continue)");
	    (void) gets(instr);
	    if (strlen(instr) != 0)
		num = atol(instr);

	    buf[0] = '\0';
	    if (rigets(h1, rec, buf, sizeof(buf)) != NULL)
		(void) printf("\"%s\"\n", buf);
	    else
		(void) printf("returns NULL\n");
	    break;

	case 'r':
	case 'R':
	    /* this is just an example - not pretty */
	    (void) printf("read record:");
	    (void) gets(instr);
	    rec = atol(instr);
	    (void) printf("how many bytes (max:%d <return> = 20):", BUFSIZ);
	    (void) gets(instr);
	    num = atoi(instr);
	    if (num == 0)
		num = 20;
	    if (num > BUFSIZ)
		num = BUFSIZ;

	    buf[0] = '\0';
	    ret = riread(h1, rec, buf, num, 0);

	    if (ret >= 0)
		(void) printf("\"%s\"\n", buf);
	    else
		(void) printf("returns %d\n", ret);
	    break;


	case 'm':
	case 'M':
	    (void) printf("how many bytes (max:%d <return>=20):", BUFSIZ);
	    (void) gets(instr);
	    num = atoi(instr);
	    if (num == 0)
		num = 20;
	    if (num > BUFSIZ)
		num = BUFSIZ;
	    buf[0] = '\0';
	    ret = riread(h1, rec, buf, num, 1);
	    if (ret > 0)
		(void) printf("\"%s\"\n", buf);
	    else
		(void) printf("returns %d\n", ret);
	    break;

	case 'p':
	case 'P':
	    (void) printf("puts to record: (<return> = same record)");
	    (void) gets(instr);
	    if (strlen(instr) != 0)
		rec = atol(instr);

	    (void) printf("enter data: ");
	    (void) gets(buf);
	    if (riputs(h1, rec, buf) == EOF)
		(void) printf("returns EOF\n");
	    else
		(void) printf(" ok\n");

	    break;

	case 'w':
	case 'W':
	    /* this is just an example - not pretty */
	    (void) printf("write to record:");
	    (void) gets(instr);
	    rec = atol(instr);
	    (void) printf("enter data: ");
	    (void) gets(buf);
	    ret = riwrite(h1, rec, buf, strlen(buf), 0);

	    (void) printf("....returns %d-", ret);
	    if (ret == strlen(buf))
		(void) printf(" ok\n");
	    else
		(void) printf(" count does not match\n");

	    break;

	case 'a':
	case 'A':
	    (void) printf("append to record:");
	    (void) gets(instr);
	    (void) printf("enter data:");
	    (void) gets(buf);
	    ret = riwrite(h1, rec, buf, strlen(buf), 1);

	    (void) printf("....returns %d-", ret);
	    if (ret == strlen(buf))
		(void) printf(" ok\n");
	    else
		(void) printf(" count does not match\n");

	    break;

	case 'h':
	case 'H':
	    (void) help();
	    break;

	case 'l':
	case 'L':
	    (void) printf("enter label:");
	    *buf = '\0';

	    (void) gets(buf);
	    ret = risetlab(h1, buf, strlen(buf));
	    if (ret == 0)
	    {
		*buf = '\0';
		if (rigetlab(h1, buf, BUFSIZ) < 0)
		{
		    (void) printf("error reading label\n");
		}
		else
		{
		    (void) printf("label is:\"%s\"\n", buf);
		}
	    }
	    else
	    {
		(void) printf("error writing label\n");
	    }
	    break;


	default:
	    (void) printf("huh?\n");
	}
    }
}
