/*
 * Copyright (C) 1988, Marcus J. Ranum, William Welch Medical Library
 * $Author$
 */

#if !defined(lint) && !defined(RCSid)
#define RCSid RCSid
static char *RCSid = "$Header$: recio.c";
#endif

/*
 * $Log$
 * Revision 1.2  1999/11/18 21:18:36  sopwith
 * Woohoo, add basic GUI object framework.
 *
 * Revision 1.1.1.1  1999/11/12 15:47:05  sopwith
 *
 *
 * Initial import.
 *
 * Revision 1.4  88/06/06  13:44:58  mjr
 * fixed the drop of occasional chars in rigets()
 *
 * Revision 1.3  88/06/06  09:58:58  mjr
 * *** empty log message ***
 *
 * Revision 1.2  88/06/02  15:35:32  mjr
 * added rigetcurpag and risetcurpag
 *
 * Revision 1.1  88/06/01  21:27:20  mjr
 * Initial revision
 *
 */

#define _GNU_SOURCE
#include	<stdio.h>
#include        <unistd.h>
#include        <string.h>
#include	"recio.h"

#include <sys/mman.h>
#define d_print g_print

/* this is actually highly dependent on a few major assumptions: */
/* 1) you can lseek() past EOF and extend with 'holes' */
/* (which fails under MSDOS, I think, but who cares about DOS) */
/* 2) when you read data from a 'hole' in a file, it will */
/* come back zeros - hence we can check the flags to */
/* see if it is free based on that. If your system does */
/* not work in this way, you're out of luck, or you have */
/* to construct large blank databases beforehand */
/* this indicates a node deleted */

#define	RI_DELETED	0
#define	RI_ACTIVE	-1L

/* as a result of using 0L as an indicator of being a free block or a */
/* null block, we have a left-over page at page #0. This can be used to */
/* store invariant information */

/* our magic number - just to be safe */
#define	RI_MAGIC	0x72252

/* record file superblock (one at head of map file) */
struct risuper
{
    gint32    magic;		/* magic number */
    gint32     reclen;		/* record chunk length (variable) */
    gint32    free;		/* head of free chain */
    gint32    last;		/* last page in file (alternate free list) */
};


/* an individual record header - one per record in map file */
/* the map file consists of nothing but the superblock and one */
/* of these critters per record */
struct rimap
{
    gint32    page;		/* assigned page if any, else 0L for free */
    gint32    tail;		/* tail of assigned page list */
};


/* a page header - one at the head of each page in page file. */
/* for now, quite rudimentary - room for more data if needed */
/* otherwise it would not be worth defining a structure */
/* - note - the use of longs here is a bit unecessary, but this */
/* way its 4 longs, and there are no alignment problems between */
/* my Sun and my VAX. Its a pain - feel free to fix it */
struct ripag
{
    gint32    recno;		/* extra to help reconstruct crashes */
    gint32    next;		/* next page pointer 0L = no next */
    gint32    deleted;		/* 0 is free, anything else is not */
    gint32    high;		/* this page highwater mark (EOF) */
};


/* the actual control structure. Includes a buffer for reading pages */
/* and a few other handy buffers for map entries, etc */
struct rifd
{
    int     fd;			/* record file descriptor */
    int     mfd;		/* free map file descriptor */

    struct risuper *sblk, sblk_spot;	/* superblock */
    /* all used to emulate stdio, sort of */
    char   *dat;		/* page record buffer (malloced in riopen()) */
    char   *gets_rptr;
    gint32  dat_left;
    gint32    curpag;		/* used to simulate consecutive reads */
    gint32    currec;		/* used to simulate consecutive reads */
    gint32     pagoff;		/* offset within current page */

    char *baseptr_rec;
    size_t recfile_len;

    char *baseptr_map;
    size_t mapfile_len;
    char rdonly;
};


/* if we wish to store our disk data in network byte order */
#ifdef	BYTEORDER
#error "Can't handle BYTEORDER right now"
#include	<sys/types.h>
#include	<netinet/in.h>
#endif

/* size of a page header (without buffer) */
#define	RI_PHSIZ	sizeof(struct ripag)

/* size of a page (with buffer) */
#define	RI_PSIZ(rf)	(sizeof(struct ripag) + (rf)->sblk->reclen)

/* size of a rifd superblock */
#define	RI_SSIZ		(sizeof(struct risuper))

/* size of a map file entry */
#define	RI_MSIZ		(sizeof(struct rimap))

static int rpaghed(RIFD rf, RIREC rec, struct ripag *blk);
static int wpaghed(RIFD rf, RIREC rec, struct ripag *blk);

/* write the rifd mapfile superblock to disk */
static int
wsuper(RIFD rf)
{
  struct risuper boge;

  if(rf->rdonly)
    return 0;

#ifdef	BYTEORDER
    boge.magic = htonl(rf->sblk->magic);
    boge.reclen = htonl(rf->sblk->reclen);
    boge.free = htonl(rf->sblk->free);
    boge.last = htonl(rf->sblk->last);
#else
    boge = *rf->sblk;
#endif

    if (rf->baseptr_map)
      {
	memcpy(rf->baseptr_map, &boge, RI_SSIZ);
      }
    else
      {
	if (lseek(rf->mfd, 0L, SEEK_SET) < 0)
	  return (-1);

	if (write(rf->mfd, (char *) &boge, RI_SSIZ) != RI_SSIZ)
	  return (-1);
      }

    return (0);
}

/* read the rifd mapfile superblock from disk */
static int
rsuper(RIFD rf)
{
  struct risuper boge;

  if (rf->baseptr_map)
    {
      memcpy(&boge, rf->baseptr_map, RI_SSIZ);
    }
  else
    {
      if (lseek(rf->mfd, 0L, SEEK_SET) < 0)
	return (-1);
      if (read(rf->mfd, (char *) &boge, RI_SSIZ) != RI_SSIZ)
	return (-1);

    }

#ifdef	BYTEORDER
    rf->sblk->magic = ntohl(boge.magic);
    rf->sblk->reclen = ntohl(boge.reclen);
    rf->sblk->free = ntohl(boge.free);
    rf->sblk->last = ntohl(boge.last);
#else
    *rf->sblk = boge;
#endif

    return (0);
}

static size_t pagesize = 0;
#define MAP_PAGE_MASK(ps) (~((ps)-1))
#define MAP_LEN(x) ((((x)+(pagesize)-1)&MAP_PAGE_MASK(pagesize)))

static void
riassure(RIFD rf, gboolean is_map, size_t length)
{
  char **ptrp;
  size_t *ptrs;
  size_t map_len, new_map_len;
  int fd;

  if(is_map)
    {
      ptrp = &rf->baseptr_map;
      ptrs = &rf->mapfile_len;
      fd = rf->mfd;
    }
  else
    {
      ptrp = &rf->baseptr_rec;
      ptrs = &rf->recfile_len;
      fd = rf->fd;
    }

  if(*ptrs >= length)
    return;

  map_len = MAP_LEN(*ptrs);
  new_map_len = MAP_LEN(length);

  if(new_map_len != map_len)
    {
      if(!rf->rdonly)
	ftruncate(fd, new_map_len);

#if 1
      munmap(*ptrp, map_len);
      *ptrp = mmap(NULL, new_map_len, PROT_READ|(rf->rdonly?0:PROT_WRITE), MAP_SHARED, fd, 0);
#else
      *ptrp = mremap(*ptrp, map_len, new_map_len, MREMAP_MAYMOVE);
#endif

      g_assert(*ptrp != MAP_FAILED);

      if (is_map)
	rf->sblk = (struct risuper *)rf->baseptr_map;
    }

  *ptrs = length;
}

/* dynamically allocate a control structure for an open rifd */
/* including opening and checking the mapfile and textfile */
RIFD
riopen(const char *path, int flags, mode_t mode, int reclen)
{
    struct rifd *rf;
    int     r;
    char cbuf[PATH_MAX];
    struct stat sbuf;
    int prot_write;

    if(!pagesize)
      pagesize = getpagesize();

    rf = g_new0(struct rifd, 1);
    rf->rdonly = ((flags & O_ACCMODE) == O_RDONLY);
    rf->sblk = &rf->sblk_spot;
    rf->mfd = rf->fd = -1;

    g_snprintf(cbuf, sizeof(cbuf), "%s.map", path);

    rf->mfd = open(cbuf, flags, mode);
    if(rf->mfd < 0)
      goto errout;

    r = read(rf->mfd, (char *) &rf->sblk_spot, RI_SSIZ);

    /* if read nothing, must be a new guy, right ? */
    if (r == 0 && !rf->rdonly)
      {
	rf->sblk->magic = RI_MAGIC;
	if (reclen == 0)
	  rf->sblk->reclen = BUFSIZ - sizeof(struct rimap);
	else
	  rf->sblk->reclen = reclen;
	rf->sblk->free = 0L;
	
	/* this keeps page 0 as a comment page */
	rf->sblk->last = 1L;

	if (wsuper(rf) == 0)
	  r = RI_SSIZ;
      }
#ifdef	BYTEORDER
    else if (r == RI_SSIZ)
      {
	/* read something, decode the numbers */
	rf->sblk->magic = ntohl(rf->sblk->magic);
	rf->sblk->reclen = ntohl(rf->sblk->reclen);
	rf->sblk->free = ntohl(rf->sblk->free);
	rf->sblk->last = ntohl(rf->sblk->last);
      }
#endif
    /* cleverly check ret value from either read or write */
    if (r != RI_SSIZ)
      goto errout;

    /* check that ole magic number */
    if (rf->sblk->magic != RI_MAGIC)
      goto errout;

    rf->dat = g_malloc(rf->sblk->reclen);

    g_snprintf(cbuf, sizeof(cbuf), "%s.rec", path);
    rf->fd = open(cbuf, flags, mode);
    if(rf->fd < 0)
      goto errout;

    if((flags & O_ACCMODE) == O_RDONLY)
      prot_write = 0;
    else
      prot_write = PROT_WRITE;

    if(fstat(rf->fd, &sbuf))
      goto errout;
    rf->recfile_len = MAX(sbuf.st_size, 1);
    if(!rf->rdonly)
      ftruncate(rf->fd, rf->recfile_len);
    rf->baseptr_rec = mmap(NULL, MAP_LEN(rf->recfile_len), PROT_READ|prot_write, MAP_SHARED, rf->fd, 0);

    if(fstat(rf->mfd, &sbuf))
      {
	munmap(rf->baseptr_rec, rf->recfile_len);
	goto errout;
      }
    rf->mapfile_len = MAX(sbuf.st_size, 1);
    if(!rf->rdonly)
      ftruncate(rf->mfd, rf->mapfile_len);
    rf->baseptr_map = mmap(NULL, MAP_LEN(rf->mapfile_len), PROT_READ|prot_write, MAP_SHARED, rf->mfd, 0);

    rf->pagoff = 0;
    rf->curpag = 0L;

    return (rf);

 errout:
    if(rf->mfd >= 0)
      close(rf->mfd);
    if(rf->fd >= 0)
      close(rf->fd);
    if(rf->dat)
    g_free(rf->dat);
    g_free(rf);

    return NULL;
}

/* close and deallocate the control structure */
int
riclose(RIFD rf)
{
    int     t = 0;

    if(!rf->rdonly)
      t = wsuper(rf);

    if (rf->baseptr_map)
      {
	msync(rf->baseptr_map, MAP_LEN(rf->mapfile_len), MS_SYNC|MS_INVALIDATE);
	munmap(rf->baseptr_map, MAP_LEN(rf->mapfile_len));

	if(!rf->rdonly)
	  ftruncate(rf->mfd, rf->mapfile_len);
      }
    if(rf->baseptr_rec)
      {
	msync(rf->baseptr_rec, MAP_LEN(rf->recfile_len), MS_SYNC|MS_INVALIDATE);
	munmap(rf->baseptr_rec, MAP_LEN(rf->recfile_len));

	if(!rf->rdonly)
	  ftruncate(rf->fd, rf->recfile_len);
      }

    close(rf->fd);
    close(rf->mfd);
    g_free(rf->dat);
    g_free(rf);

    return t;
}

/* allocate a page block, either by using the free page chain or */
/* by creating a new page at the end of the file */
static long
pagalloc(RIFD rf, RIREC rec)
{
    struct ripag pag;
    long    nfree;

    /* read superblock in case someone else has changed it */
    /* this is not totally bulletproof, but doubtless helps */
    if (rsuper(rf) < 0)
	return (-1);

    /* if there are free blocks, use them, otherwise use the end of file */
    if (rf->sblk->free != 0L)
    {
	/* allocate the block, and then reset the free pointer */
	if (rpaghed(rf, rf->sblk->free, &pag) < 0)
	    return (-1);

	/* should never happen ! this block is marked as in */
	/* use but is on the free list ! */
	/* (the other alternative is to just eat through the */
	/* file, using allocated blocks instead of the free */
	/* list :-)) */
	if (pag.deleted != RI_DELETED)
	{
	    /* AIIIEEEEEE!!!! */
	    return (-1);
	}

	/* remember it */
	nfree = rf->sblk->free;

	/* the new free pointer is the free block successor */
	rf->sblk->free = pag.next;
    }
    else
    {
	/* use page after current highest page in file */
	/* this is dependent on your OS being UNIX-like */
	/* in that 'holes' in files are filled with 0s */
	/* rather than something else - hence we expect */
	/* that data blocks in a 'hole' will read as all 0 */
	/* this should blow up MS-DOS */
	nfree = rf->sblk->last++;
    }

    /* all is OK - rewrite our superblock - new free head */
    if (wsuper(rf) < 0)
	return (-1);

    /* we now have removed the free entry from the list */
    /* and mark it as active */
    pag.deleted = RI_ACTIVE;
    pag.next = 0L;
    pag.recno = rec;
    pag.high = 0;
    if (wpaghed(rf, nfree, &pag) < 0)
	return (-1);

    return (nfree);
}

/* write a map block from the given struct */
static int
wmapblk(RIFD rf, RIREC rec, struct rimap *blk)
{
  struct rimap boge;

  if(rf->rdonly)
    return -1;
  
#ifdef	BYTEORDER
  boge.page = htonl(blk->page);
  boge.tail = htonl(blk->tail);
#else
  boge = *blk;
#endif
  if(rf->baseptr_map)
    {
      size_t off;

      off = (rec * RI_MSIZ) + RI_SSIZ;

      riassure(rf, TRUE, off + RI_MSIZ);

      memcpy(rf->baseptr_map + off, &boge, RI_MSIZ);
    }
  else
    {

      if (lseek(rf->mfd, (rec * RI_MSIZ) + RI_SSIZ, SEEK_SET) < 0)
	return (-1);


      if (write(rf->mfd, (char *) &boge, RI_MSIZ) != RI_MSIZ)
	return (-1);
    }

  return (0);
}

/* read a map block into the given struct */
static int
rmapblk(RIFD rf, RIREC rec, struct rimap *blk)
{
    int     ret;

    if (rf->baseptr_map)
      {
	size_t off;

	off = (rec * RI_MSIZ) + RI_SSIZ;
	if((off + RI_MSIZ) > rf->mapfile_len)
	  {
	    blk->page = blk->tail = 0;
	    return 0;
	  }

	memcpy(blk, rf->baseptr_map + off, RI_MSIZ);
#ifdef BYTEORDER
	blk->page = ntohl(blk->page);
	blk->tail = ntohl(blk->tail);
#endif
      }
    else
      {
#ifdef	BYTEORDER
	struct rimap boge;

#endif

	if (lseek(rf->mfd, (rec * RI_MSIZ) + RI_SSIZ, SEEK_SET) < 0)
	  return (-1);

#ifdef	BYTEORDER
	ret = read(rf->mfd, (char *) &boge, RI_MSIZ);
	blk->page = ntohl(boge.page);
	blk->tail = ntohl(boge.tail);
#else
	ret = read(rf->mfd, (char *) blk, RI_MSIZ);
#endif

	/* if we EOFfed we are still (maybe) OK */
	if (ret == 0)
	  {
	    blk->page = 0L;
	    blk->tail = 0L;
	    ret = RI_MSIZ;		/* kluge */
	  }

	if (ret != RI_MSIZ)
	  return (-1);
      }

    return (0);
}

/* read a page header into the given struct */
static int
rpaghed(RIFD rf, RIREC rec, struct ripag *blk)
{
    int     r;

    if (rf->baseptr_rec)
      {
	size_t off;

	off = (rec * RI_PSIZ(rf));
	if ((off + RI_PHSIZ) > rf->recfile_len)
	  {
	    blk->recno = rec;
	    blk->next = blk->high = 0;
	    blk->deleted = RI_DELETED;
	    return 0;
	  }

	memcpy(blk, rf->baseptr_rec + off, RI_PHSIZ);
#ifdef BYTEORDER
	blk->recno = ntohl(blk->recno);
	blk->next = ntohl(blk->next);
	blk->high = ntohl(blk->high);
	blk->deleted = ntohl(blk->deleted);
#endif
      }
    else
      {
#ifdef	BYTEORDER
	struct ripag boge;

#endif

	/* seek the distance, and read a page header */
	if (lseek(rf->fd, (rec * RI_PSIZ(rf)), SEEK_SET) < 0)
	  return (-1);

#ifdef	BYTEORDER
	r = read(rf->fd, (char *) &boge, RI_PHSIZ);
	blk->recno = ntohl(boge.recno);
	blk->next = ntohl(boge.next);
	blk->high = ntohl(boge.high);
	blk->deleted = ntohl(boge.deleted);
#else
	/* try to read a page */
	r = read(rf->fd, (char *) blk, RI_PHSIZ);
#endif

	/* if we EOFfed, we are still (maybe) OK */
	if (r == 0)
	  {
	    blk->recno = rec;
	    blk->next = 0L;
	    blk->high = 0;
	    blk->deleted = RI_DELETED;
	    r = RI_PHSIZ;		/* kluge */
	  }

	if (r != RI_PHSIZ)
	  return (-1);
      }

    return (0);
}

/* write a page header from the given struct */
static int
wpaghed(RIFD rf, RIREC rec, struct ripag *blk)
{
  struct ripag boge;

#ifdef	BYTEORDER
  boge.recno = htonl(blk->recno);
  boge.next = htonl(blk->next);
  boge.high = htonl(blk->high);
  boge.deleted = htonl(blk->deleted);
#else
  boge = *blk;
#endif

  if(rf->baseptr_rec)
    {
      size_t off = (rec * RI_PSIZ(rf));

      riassure(rf, FALSE, off + RI_PHSIZ);
      memcpy(rf->baseptr_rec + off, &boge, RI_PHSIZ);
    }
  else
    {

      if (lseek(rf->fd, (rec * RI_PSIZ(rf)), SEEK_SET) < 0)
	return (-1);
#ifdef	BYTEORDER
      if (write(rf->fd, (char *) &boge, RI_PHSIZ) != RI_PHSIZ)
	return (-1);
#else
      if (write(rf->fd, (char *) blk, RI_PHSIZ) != RI_PHSIZ)
	return (-1);
#endif
    }

    return (0);
}

int
riwrite(RIFD rf, RIREC rec, const void *buf, size_t count, gboolean append)
{
    long    j;			/* junk */
    int     k;			/* junk */
    int     wrote = 0;		/* number of char written */
    long    this = 0L;		/* current page */
    struct rimap map;		/* buffer for map entries */
    struct ripag pag;		/* buffer for page entries */

    if(rf->rdonly)
      return -1;

    /* no matter what, we will need our map block */
    if (rmapblk(rf, rec, &map) < 0)
      {
	return (-1);
      }

    /* turn off read continuation for this record */
    rf->curpag = -1L;

    /* if no page at start, write map to disk now - new page */
    if (map.page == 0L)
    {
	map.page = pagalloc(rf, rec);
	if (map.page == -1L)
	    return (-1);
	if (wmapblk(rf, rec, &map) < 0)
	    return (-1);
    }


    /* if we are appending, we start at the last page and continue */
    if (append)
    {
	if (map.tail != 0)
	    this = map.tail;
	else
	    this = map.page;
    }
    else
    {
	/* start at the first page */
	this = map.page;
    }

    /* main loop, in which we spit the bytes to disk */
    while (count > 0)
    {
	/* read our page header */
	if (rpaghed(rf, this, &pag) < 0)
	    return (-1);

	/* seek to head of page data segment possibly */
	/* skipping over previously written bytes */
	if (append)
	    j = (this * (long) RI_PSIZ(rf)) + RI_PHSIZ + pag.high;
	else
	    j = (this * (long) RI_PSIZ(rf)) + RI_PHSIZ;

	/* actually do the thing */
	if(! rf->baseptr_rec)
	  {
	    if (lseek(rf->fd, j, SEEK_SET) < 0)
	      return (-1);
	  }

	/* figure how much to write */
	/* is this devo ?? - my head hurtz */
	if (append)
	{
	    if (count > rf->sblk->reclen - pag.high)
		k = rf->sblk->reclen - pag.high;
	    else
		k = count;
	}
	else
	{
	    if (count > rf->sblk->reclen)
		k = rf->sblk->reclen;
	    else
		k = count;
	}

	if (rf->baseptr_rec)
	  {
	    riassure (rf, FALSE, j + k);
	    memcpy(rf->baseptr_rec + j, ((char *)buf) + wrote, k);
	  }
	else
	  {
	    /* actually do the thing */
	    if (write(rf->fd, ((char *)buf) + wrote, k) != k)
	      return (-1);
	  }

	/* adjust our notion of where we are and so on */
	wrote += k;
	count -= k;

	if (append)
	{
	    pag.high += k;
	}
	else
	{
	    /* this prevents us from accidentally losing the */
	    /* rest of the page in overwrites */
	    if (k > pag.high)
		pag.high = k;
	}

	/* remember our new last page in chain is this one */
	map.tail = this;

	if (count > 0)
	{
	    /* the current block has no associated page */
	    if (pag.next == 0L)
	    {
		pag.next = pagalloc(rf, rec);
		if (pag.next == -1L)
		    return (-1);
	    }

	    /* write our page header with next pointer */
	    if (wpaghed(rf, this, &pag) < 0)
		return (-1);

	    /* move along to next page */
	    this = pag.next;
	}
    }

    /* write our page header */
    if (wpaghed(rf, this, &pag) < 0)
	return (-1);

    /* write our map block */
    if (wmapblk(rf, rec, &map) < 0)
	return (-1);

    return (wrote);
}

/* drop a list of pages, and pop them onto the free list */
int
riunlink(RIFD rf, RIREC rec)
{
    long    head = 0L;		/* first page - we build a chain */
    long    this = 0L;		/* current page */
    struct rimap map;		/* buffer for map entries */
    struct ripag pag;		/* buffer for page entries */

    /* read superblock in case someone else has changed it */
    if (rsuper(rf) < 0)
	return (-1);

    /* turn off read continuation if this record */
    if (rec == rf->currec)
	rf->curpag = -1L;

    /* now read the mapblock to be deleted */
    if (rmapblk(rf, rec, &map) < 0)
	return (-1);

    /* the map block has no associated page block - life is easy */
    if (map.page == 0L)
	return (0);

    /* remember our page */
    this = map.page;
    head = map.page;

    map.page = 0L;
    map.tail = 0L;

    /* first off, mark the thing as gone */
    if (wmapblk(rf, rec, &map) < 0)
	return (-1);

    while (1)
    {
	/* read our page header */
	if (rpaghed(rf, this, &pag) < 0)
	    return (-1);

	/* if no next page, we are mostly done */
	/* drop to bottom, which links us into the free chain */
	if (pag.next == 0L)
	    break;

	/* mark it and go to next */
	pag.deleted = RI_DELETED;
	if (wpaghed(rf, this, &pag) < 0)
	    return (-1);

	this = pag.next;
    }

    /* this now points at the end of our list */
    /* head points at the first one. so we: */

    /* make the superblock next free block pointer the next of our */
    /* last node - if we crash here we lose our free list - they just */
    /* sit there */
    pag.next = rf->sblk->free;

    pag.deleted = RI_DELETED;
    if (wpaghed(rf, this, &pag) < 0)
	return (-1);

    /* now the superblock free list points to our list head */
    rf->sblk->free = head;
    if (wsuper(rf) < 0)
	return (-1);

    return (0);
}

int
riread(RIFD rf, RIREC rec, void *buf, size_t count, gboolean cont)
{
    long    j;			/* junk */
    int     k;			/* junk */
    long    this;
    int     nread = 0;		/* number of char read */
    struct rimap map;		/* buffer for map entries */
    struct ripag pag;		/* buffer for page entries */

    /* these check to make sure our current record and such are */
    /* all still more or less valid - a kludge, but it works... */
    if (rec != rf->currec)
    {
	cont = 0;
	rf->currec = rec;
    }

    if (cont)
    {
	if (rf->curpag != -1L)
	    this = rf->curpag;
	else
	  cont = FALSE;
    }

    if (!cont)
    {
	/* we are starting a new record - read the map block */
	if (rmapblk(rf, rec, &map) < 0)
	    return (-1);

	this = map.page;
	rf->curpag = 0L;
	rf->pagoff = 0;
    }

    while (count > 0)
    {

	/* we have run out of pages to read */
	if (this == 0L)
	    break;

	/* read our page header */
	if (rpaghed(rf, this, &pag) < 0)
	    return (-1);

	/* seek to head of page data segment */
	if (cont)
	    j = (this * (long) RI_PSIZ(rf)) + RI_PHSIZ + rf->pagoff;
	else
	    j = (this * (long) RI_PSIZ(rf)) + RI_PHSIZ;

	if (!rf->baseptr_rec)
	  {
	    if (lseek(rf->fd, j, SEEK_SET) < 0)
	      return (-1);
	  }

	/* read a page worth OR whatever is left in that page */
	if (cont)
	{
	    if (count >= (pag.high - rf->pagoff))
	    {
		/* read rest of page, go to next */
		k = pag.high - rf->pagoff;
		this = pag.next;
		rf->curpag = this;
	    }
	    else
	    {
		k = count;
	    }
	}
	else
	{
	    if (count < pag.high)
	    {
		k = count;
	    }
	    else
	    {
		this = pag.next;
		rf->curpag = this;
		k = pag.high;
	    }
	}

	if(rf->baseptr_rec)
	  {
	    if ((j + k) > rf->recfile_len)
	      return -1;

	    memcpy(((char *)buf) + nread, rf->baseptr_rec + j, k);
	  }
	else
	  {
	    if (read(rf->fd, ((char *)buf) + nread, k) < 0)
	      return (-1);
	  }

	/* adjust our notion of where we are */
	nread += k;
	count -= k;
	rf->pagoff += k;
	if (rf->pagoff >= rf->sblk->reclen)
	{
	    rf->pagoff -= rf->sblk->reclen;
	}
    }

    rf->curpag = this;
    if(nread < count)
      memset(((char *)buf) + nread, 0, 1);

    return (nread);
}

/* set the record file label (page 0) from the contents of buf */
int
risetlab(RIFD rf, const void *buf, size_t count)
{
    /* will it fit ? */
    if (count > rf->sblk->reclen)
	return (-1);

    if(rf->baseptr_rec)
      {
	riassure(rf, FALSE, RI_PHSIZ + count);
	memcpy(rf->baseptr_rec + RI_PHSIZ, buf, count);
      }
    else
      {
	if (lseek(rf->fd, (long) RI_PHSIZ, SEEK_SET) < 0)
	  return (-1);
	if (write(rf->fd, buf, count) != count)
	  return (-1);
      }

    return (0);
}


/* get the record file label (page 0) into buf */
int
rigetlab(RIFD rf, void *buf, size_t count)
{
  if (count > rf->sblk->reclen)
    return (-1);

  if (rf->baseptr_rec)
    {
      if(rf->recfile_len < (RI_PHSIZ + count))
	return -1;

      memcpy(buf, rf->baseptr_rec + RI_PHSIZ, count);
    }
  else
  {
    if (lseek(rf->fd, (long) RI_PHSIZ, SEEK_SET) < 0)
	return (-1);

    if (read(rf->fd, buf, count) < 0)
	return (-1);
  }

  return (0);
}

char   *
rigets(RIFD rf, RIREC rec, char *buf, size_t buf_size)
{
    char   *bptr = buf;
    gboolean cont;

    /* this is kind of wasteful, but necessary */
    /* we re-check to make sure that the record has not changed */
    /* in riread() - but have to do it here, too, to make sure */
    /* that we can take from our buffer, instead of reading */
    cont = (rec == rf->currec);

    do {
      /* if the record number has changed, or the buffer is empty */
      /* we must needs fill us a buffer */
      if(!cont || !rf->gets_rptr
	 || rf->gets_rptr >= (rf->dat + rf->dat_left))
	{
	  rf->dat_left = riread(rf, rec, rf->dat, rf->sblk->reclen, TRUE);
	  if (rf->dat_left <= 0)
	    {
	      /* record empty, finish buffer */
	      *bptr = '\0';
	      return (buf);
	    }
	    rf->gets_rptr = rf->dat;
	    cont = TRUE;
	}

	switch (*rf->gets_rptr)
	{

	case '\0':
	  *bptr = '\0';
	  rf->gets_rptr++;
	  return buf;
	  break;

	case '\n':
	  *bptr = '\0';
	  rf->gets_rptr++;
	  return (buf);

	default:
	  *bptr++ = *rf->gets_rptr++;
	}

    } while (bptr < (buf + buf_size - 1));

    buf[buf_size - 1] = '\0';

    return buf;
}

int
riputs(RIFD rf, RIREC rec, const char *buf)
{
    riwrite(rf, rec, buf, strlen(buf), TRUE);
    riwrite(rf, rec, "\n", 1, TRUE);

    return (0);
}

int
rigetcurpag(RIFD rf, char *buf)
{
    if (rf->curpag <= 0L)
	return (-1);

    if(rf->baseptr_rec)
      {
	size_t off = (rf->curpag * (long) RI_PSIZ(rf)) + RI_PHSIZ;

	if((off + rf->sblk->reclen) > rf->recfile_len)
	  return -1;

	memcpy(buf, rf->baseptr_rec + off, rf->sblk->reclen);
      }
    else
      {
	if (lseek(rf->fd, (rf->curpag * (long) RI_PSIZ(rf)) + RI_PHSIZ, 0) < 0)
	  return (-1);
	if (read(rf->fd, buf, rf->sblk->reclen) != rf->sblk->reclen)
	  return (-1);
      }
    return (0);
}

int
risetcurpag(RIFD rf, char *buf)
{
    if (rf->curpag <= 0L)
	return (-1);

    if (rf->baseptr_rec)
      {
	size_t off = (rf->curpag * (long) RI_PSIZ(rf)) + RI_PHSIZ;

	riassure(rf, FALSE, off + rf->sblk->reclen);
	memcpy(rf->baseptr_rec + off, buf, rf->sblk->reclen);
      }
    else
      {
	if (lseek(rf->fd, (rf->curpag * (long) RI_PSIZ(rf)) + RI_PHSIZ, 0) < 0)
	  return (-1);
	if (write(rf->fd, buf, rf->sblk->reclen) != rf->sblk->reclen)
	  return (-1);
      }

    return (0);
}
