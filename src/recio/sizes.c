/*
 * Copyright (C) 1988, Marcus J. Ranum, William Welch Medical Library
 * $Author$
 */

#if !defined(lint) && !defined(RCSid)
#define RCSid RCSid
static char *RCSid = "$Header$: sizes.c";

#endif

/*
 * $Log$
 * Revision 1.1  1999/11/12 15:47:05  sopwith
 * Initial revision
 *
 * Revision 1.1  88/06/01  21:27:31  mjr
 * Initial revision
 *
 */

/* provides some minimally useful info about how big your */
/* data structures are */

#include "recio.c"

int main(void)
{
    printf("\na map block is %d bytes\n", sizeof(struct rimap));
    printf("\na page header is %d bytes\n", sizeof(struct ripag));
    printf("make sure these sizes match, if you're running on\n");
    printf("several different types of CPUs.\n");
    return 0;
}
