#include "recio.h"
#include <glib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

int main(int argc, char *argv[])
{
	char aline[4096];
	RIFD rifd;
	int i = 1, max = 0;
	char *fn = "testit";

	if(argc > 1) fn = argv[1];

	rifd = riopen(fn, O_CREAT|O_RDWR, 0664, 40);
	g_assert(rifd);
	while(fgets(aline, sizeof(aline), stdin)) {
		g_strstrip(aline);
		max += strlen(aline);
		riwrite(rifd, i, aline, strlen(aline), FALSE);
		i++;
	}
	riclose(rifd);
	g_print("Avg size is %f\n", ((double)max)/((double)i));
	return 0;
}
