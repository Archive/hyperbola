#include "recio.h"
#include <glib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

int main(void)
{
	char aline[4096];
	RIFD rifd;
	int i, nr;

	rifd = riopen("testit", O_RDONLY, 0664, 48);
	g_assert(rifd);
	for(i = nr = 1; nr; i++) {
		nr = riread(rifd, i, aline, sizeof(aline), FALSE);
		if(nr <= 0)
		  break;
		aline[nr] = '\0';
		g_print("%d: %s (%d)\n", i, aline, nr);
	}
	riclose(rifd);
	return 0;
}
