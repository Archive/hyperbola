#include "config.h"

#include <gnome.h>
#include "hyperbola.h"

typedef struct {
  HyperbolaWindow *appwin;
  HyperbolaView *locview;
} HBAppInfo;

int main(int argc, char *argv[])
{
  HBAppInfo hai;
  HyperbolaNavigationContext dummy_ctx;

  gnome_init("hyperbola", VERSION, argc, argv);

  hai.appwin = HYPERBOLA_WINDOW(gtk_widget_new(hyperbola_window_get_type(), "app_id", "hyperbola", "title", "Hyperbola", NULL));
  gtk_signal_connect(GTK_OBJECT(hai.appwin), "destroy", GTK_SIGNAL_FUNC(gtk_main_quit), NULL);

  hyperbola_window_set_default_state(hai.appwin);

  gtk_widget_show(GTK_WIDGET(hai.appwin));

  dummy_ctx.initiating_view = NULL;
  dummy_ctx.initiating_data = NULL;
  if(argc > 1)
    hyperbola_window_request_location_change(hai.appwin, argv[1], &dummy_ctx);
  else
    hyperbola_window_request_location_change(hai.appwin, "ghelp:", &dummy_ctx);

  gtk_main();

  return 0;
}
