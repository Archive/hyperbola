#ifndef HYPERBOLA_NAV_HISTORY_H
#define HYPERBOLA_NAV_HISTORY_H 1

#include "hyperbola-nav-view.h"

#define HYPERBOLA_TYPE_NAVIGATION_HISTORY (hyperbola_navigation_history_get_type())
#define HYPERBOLA_NAVIGATION_HISTORY(obj)	        (GTK_CHECK_CAST ((obj), HYPERBOLA_TYPE_NAVIGATION_HISTORY, HyperbolaNavigationHistory))
#define HYPERBOLA_NAVIGATION_HISTORY_CLASS(klass)      (GTK_CHECK_CLASS_CAST ((klass), HYPERBOLA_TYPE_NAVIGATION_HISTORY, HyperbolaNavigationHistoryClass))
#define HYPERBOLA_IS_NAVIGATION_HISTORY(obj)	        (GTK_CHECK_TYPE ((obj), HYPERBOLA_TYPE_NAVIGATION_HISTORY))
#define HYPERBOLA_IS_NAVIGATION_HISTORY_CLASS(klass)   (GTK_CHECK_CLASS_TYPE ((klass), HYPERBOLA_TYPE_NAVIGATION_HISTORY))

typedef struct _HyperbolaNavigationHistory HyperbolaNavigationHistory;

typedef struct {
  HyperbolaNavigationViewClass parent_spot;

  HyperbolaNavigationViewClass *parent_class;
} HyperbolaNavigationHistoryClass;

struct _HyperbolaNavigationHistory {
  HyperbolaNavigationView parent_object;

  GtkWidget *url_list;
  gint notify_count, last_row;
};

GtkType hyperbola_navigation_history_get_type(void);

#endif
