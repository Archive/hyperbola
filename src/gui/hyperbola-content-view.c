#include "hyperbola-content-view.h"
#include <gtk/gtksignal.h>

static void hyperbola_content_view_class_init (HyperbolaContentViewClass *klass);
static void hyperbola_content_view_init (HyperbolaContentView *view);

GtkType
hyperbola_content_view_get_type(void)
{
  static guint view_type = 0;

  if (!view_type)
    {
      GtkTypeInfo view_info = {
	"HyperbolaContentView",
	sizeof(HyperbolaContentView),
	sizeof(HyperbolaContentViewClass),
	(GtkClassInitFunc) hyperbola_content_view_class_init,
	(GtkObjectInitFunc) hyperbola_content_view_init
      };

      view_type = gtk_type_unique (hyperbola_view_get_type(), &view_info);
    }

  return view_type;
}

static void
hyperbola_content_view_class_init (HyperbolaContentViewClass *klass)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;

  object_class = (GtkObjectClass*) klass;
  widget_class = (GtkWidgetClass*) klass;
  klass->parent_class = gtk_type_class (gtk_type_parent (object_class->type));
}

static void
hyperbola_content_view_init (HyperbolaContentView *view)
{
}

HyperbolaContentView *
hyperbola_content_view_new(void)
{
  return HYPERBOLA_CONTENT_VIEW (gtk_type_new (hyperbola_content_view_get_type()));
}
