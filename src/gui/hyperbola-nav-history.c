#include "config.h"
#include <gnome.h>
#include "hyperbola-nav-history.h"

static void hyperbola_navigation_history_class_init (HyperbolaNavigationHistoryClass *klass);
static void hyperbola_navigation_history_init (HyperbolaNavigationHistory *view);
static void hyperbola_navigation_history_notify_location_change (HyperbolaView *view,
								 const HyperbolaLocationInfo *loci,
								 HyperbolaView *content_view,
								 HyperbolaNavigationContext *nav_context);
static void hyperbola_navigation_history_select_row(GtkCList *clist, gint row, gint column, GdkEvent *event,
						    gpointer data);


GtkType
hyperbola_navigation_history_get_type(void)
{
  static guint view_type = 0;

  if (!view_type)
    {
      const GtkTypeInfo view_info = {
	"HyperbolaNavigationHistory",
	sizeof(HyperbolaNavigationHistory),
	sizeof(HyperbolaNavigationHistoryClass),
	(GtkClassInitFunc) hyperbola_navigation_history_class_init,
	(GtkObjectInitFunc) hyperbola_navigation_history_init
      };

      view_type = gtk_type_unique (hyperbola_navigation_view_get_type(), &view_info);
    }

  return view_type;
}

static void
hyperbola_navigation_history_class_init (HyperbolaNavigationHistoryClass *klass)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;
  HyperbolaViewClass *view_class;

  object_class = (GtkObjectClass*) klass;

  widget_class = (GtkWidgetClass*) klass;

  view_class = (HyperbolaViewClass *) klass;
  view_class->notify_location_change = hyperbola_navigation_history_notify_location_change;

  klass->parent_class = gtk_type_class (gtk_type_parent (object_class->type));
  ((HyperbolaNavigationViewClass *)klass)->nav_description = N_("History");
}

static void
hyperbola_navigation_history_init (HyperbolaNavigationHistory *view)
{
  static const char *col_titles[2] = {};
  GtkWidget *wtmp;

  col_titles[0] = _("Title");
  col_titles[1] = _("Path");
  view->url_list = gtk_clist_new_with_titles(2, (char **)col_titles);
  gtk_signal_connect(GTK_OBJECT(view->url_list), "select_row", hyperbola_navigation_history_select_row,
		     view);
  gtk_clist_set_selection_mode(GTK_CLIST(view->url_list), GTK_SELECTION_BROWSE);
#if 0
  gtk_clist_column_titles_passive(GTK_CLIST(view->url_list));
#endif

  gtk_widget_set_usize(view->url_list, gtk_clist_columns_autosize(GTK_CLIST(view->url_list)), 200);

  wtmp = gtk_scrolled_window_new(gtk_clist_get_hadjustment(GTK_CLIST(view->url_list)),
				 gtk_clist_get_vadjustment(GTK_CLIST(view->url_list)));
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(wtmp), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_container_add(GTK_CONTAINER(view), wtmp);

  gtk_container_add(GTK_CONTAINER(wtmp), view->url_list);

  gtk_widget_show_all(wtmp);
}

static void
hyperbola_navigation_history_notify_location_change (HyperbolaView *view,
						     const HyperbolaLocationInfo *loci,
						     HyperbolaView *content_view,
						     HyperbolaNavigationContext *nav_context)
{
  HyperbolaNavigationHistory *hview;
  char *cols[2];
  int new_rownum;
  GtkCList *clist;

  if(nav_context->initiating_view == ((GtkWidget *)view))
    return; /* ignore, change already made */

  hview = HYPERBOLA_NAVIGATION_HISTORY(view);

  hview->notify_count++;

  clist = GTK_CLIST(hview->url_list);

  if(hview->last_row > 0)
    {
      char *uri;
      int i;

      /* If we are moving 'forward' in history, must either just select a new row that is farther ahead in history,
	 or delete all the history ahead of this point */
      gtk_clist_get_text(clist, hview->last_row - 1, 1, &uri);
      if(!strcmp(uri, loci->uri))
	{
	  new_rownum = --hview->last_row;
	  goto skip_prepend;
	}

      for(i = 0; i <= hview->last_row; i++)
	gtk_clist_remove(clist, 0);
    }

  gtk_clist_freeze(clist);
  cols[0] = loci->page_title?loci->page_title:(char*)loci->uri;
  cols[1] = (char *)loci->uri;
  hview->last_row = new_rownum = gtk_clist_prepend(clist, cols);

 skip_prepend:
  gtk_clist_columns_autosize(clist);

  if(gtk_clist_row_is_visible(clist, new_rownum) != GTK_VISIBILITY_FULL)
    gtk_clist_moveto(clist, new_rownum, -1, 0.5, 0.0);

  gtk_clist_select_row(clist, new_rownum, 0);

  gtk_clist_thaw(clist);

  hview->notify_count--;
}

static void
hyperbola_navigation_history_select_row(GtkCList *clist, gint row, gint column, GdkEvent *event,
					gpointer data)
{
  HyperbolaNavigationHistory *hview = HYPERBOLA_NAVIGATION_HISTORY(data);
  char *uri;

  if(hview->notify_count > 0)
    return;

  gtk_clist_freeze(clist);

  if(hview->last_row == row)
    return;

  hview->last_row = row;

  if(gtk_clist_row_is_visible(clist, row) != GTK_VISIBILITY_FULL)
    gtk_clist_moveto(clist, row, -1, 0.5, 0.0);

  uri = NULL;
  gtk_clist_get_text(clist, row, 1, &uri);

  hyperbola_view_request_location_change(HYPERBOLA_VIEW(data), uri, NULL);

  gtk_clist_thaw(clist);
}
