/* hyperbola-view.h
 * Copyright (C) 1999  Jonathan Blandford
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __HYPERBOLA_VIEW_H__
#define __HYPERBOLA_VIEW_H__

#include <gtk/gtkwidget.h>
#include <gtk/gtkbin.h>
#include "hyperbola-types.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define HYPERBOLA_TYPE_VIEW			(hyperbola_view_get_type ())
#define HYPERBOLA_VIEW(obj)			(GTK_CHECK_CAST ((obj), HYPERBOLA_TYPE_VIEW, HyperbolaView))
#define HYPERBOLA_VIEW_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), HYPERBOLA_TYPE_VIEW, HyperbolaViewClass))
#define HYPERBOLA_IS_VIEW(obj)			(GTK_CHECK_TYPE ((obj), HYPERBOLA_TYPE_VIEW))
#define HYPERBOLA_IS_VIEW_CLASS(klass)		(GTK_CHECK_CLASS_TYPE ((obj), HYPERBOLA_TYPE_VIEW))


typedef struct _HyperbolaView       HyperbolaView;
typedef struct _HyperbolaViewClass  HyperbolaViewClass;

struct _HyperbolaView
{
	GtkBin parent;

	GtkWidget *main_window;
};

struct _HyperbolaViewClass
{
	GtkBinClass parent_spot;

	void (*notify_location_change)	(HyperbolaView *view,
					 const HyperbolaLocationInfo *loci,
					 HyperbolaView *content_view,
					 HyperbolaNavigationContext *nav_context);
        void (*load_state) (HyperbolaView *view, const char *config_path);
        void (*save_state) (HyperbolaView *view, const char *config_path);
        void (*show_properties) (HyperbolaView *view);

        GtkBinClass *parent_class;
        guint view_signals[3];
};

GtkType hyperbola_view_get_type                (void);
void    hyperbola_view_construct               (HyperbolaView              *view,
						GtkWidget                  *main_window);
void    hyperbola_view_request_location_change (HyperbolaView              *view,
						HyperbolaLocationReference  loc,
						gpointer                    nav_context_data);




#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __HYPERBOLA_VIEW_H__ */
