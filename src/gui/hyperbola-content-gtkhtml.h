#ifndef HYPERBOLA_CONTENT_GTKHTML_H
#define HYPERBOLA_CONTENT_GTKHTML_H 1

#include "hyperbola-content-view.h"

#define HYPERBOLA_TYPE_CONTENT_GTKHTML (hyperbola_content_gtkhtml_get_type())
#define HYPERBOLA_CONTENT_GTKHTML(obj)	        (GTK_CHECK_CAST ((obj), HYPERBOLA_TYPE_CONTENT_GTKHTML, HyperbolaContentGtkHTML))
#define HYPERBOLA_CONTENT_GTKHTML_CLASS(klass)      (GTK_CHECK_CLASS_CAST ((klass), HYPERBOLA_TYPE_CONTENT_GTKHTML, HyperbolaContentGtkHTMLClass))
#define HYPERBOLA_IS_CONTENT_GTKHTML(obj)	        (GTK_CHECK_TYPE ((obj), HYPERBOLA_TYPE_CONTENT_GTKHTML))
#define HYPERBOLA_IS_CONTENT_GTKHTML_CLASS(klass)   (GTK_CHECK_CLASS_TYPE ((klass), HYPERBOLA_TYPE_CONTENT_GTKHTML))

typedef struct {
  HyperbolaContentViewClass parent_spot;

  HyperbolaContentViewClass *parent_class;
} HyperbolaContentGtkHTMLClass;

typedef struct {
  HyperbolaContentView parent_object;

  GtkWidget *htmlw, *scrollwin;

  char *cur_base, *cur_baseref;
  guint notify_count;
  FILE *html_content;
} HyperbolaContentGtkHTML;

GtkType hyperbola_content_gtkhtml_get_type(void);
HyperbolaContentGtkHTML *hyperbola_content_gtkhtml_new(void);

#endif
