#ifndef HYPERBOLA_NAV_LOCATION_H
#define HYPERBOLA_NAV_LOCATION_H 1

#include "hyperbola-nav-view.h"

#define HYPERBOLA_TYPE_NAVIGATION_LOCATION (hyperbola_navigation_location_get_type())
#define HYPERBOLA_NAVIGATION_LOCATION(obj)	        (GTK_CHECK_CAST ((obj), HYPERBOLA_TYPE_NAVIGATION_LOCATION, HyperbolaNavigationLocation))
#define HYPERBOLA_NAVIGATION_LOCATION_CLASS(klass)      (GTK_CHECK_CLASS_CAST ((klass), HYPERBOLA_TYPE_NAVIGATION_LOCATION, HyperbolaNavigationLocationClass))
#define HYPERBOLA_IS_NAVIGATION_LOCATION(obj)	        (GTK_CHECK_TYPE ((obj), HYPERBOLA_TYPE_NAVIGATION_LOCATION))
#define HYPERBOLA_IS_NAVIGATION_LOCATION_CLASS(klass)   (GTK_CHECK_CLASS_TYPE ((klass), HYPERBOLA_TYPE_NAVIGATION_LOCATION))

typedef struct _HyperbolaNavigationLocation HyperbolaNavigationLocation;

typedef struct {
  HyperbolaNavigationViewClass parent_spot;

  HyperbolaNavigationViewClass *parent_class;

} HyperbolaNavigationLocationClass;

struct _HyperbolaNavigationLocation {
  HyperbolaNavigationView parent_object;

  GtkWidget *entry;
};

GtkType hyperbola_navigation_location_get_type(void);
HyperbolaNavigationLocation *hyperbola_navigation_location_new(void);

#endif
