#include "hyperbola-nav-search.h"
#include <gtk/gtksignal.h>

static void hyperbola_navigation_search_class_init (HyperbolaNavigationSearchClass *klass);
static void hyperbola_navigation_search_init (HyperbolaNavigationSearch *view);

GtkType
hyperbola_navigation_search_get_type(void)
{
  static guint view_type = 0;

  if (!view_type)
    {
      GtkTypeInfo view_info = {
	"HyperbolaNavigationSearch",
	sizeof(HyperbolaNavigationSearch),
	sizeof(HyperbolaNavigationSearchClass),
	(GtkClassInitFunc) hyperbola_navigation_search_class_init,
	(GtkObjectInitFunc) hyperbola_navigation_search_init
      };

      view_type = gtk_type_unique (hyperbola_navigation_view_get_type(), &view_info);
    }

  return view_type;
}

static void
hyperbola_navigation_search_class_init (HyperbolaNavigationSearchClass *klass)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;

  object_class = (GtkObjectClass*) klass;
  widget_class = (GtkWidgetClass*) klass;
  klass->parent_class = gtk_type_class (gtk_type_parent (object_class->type));

  ((HyperbolaNavigationViewClass *)klass)->nav_description = N_("Search");
}

static void
hyperbola_navigation_search_init (HyperbolaNavigationSearch *view)
{
}

HyperbolaNavigationSearch *
hyperbola_navigation_search_new(void)
{
  return HYPERBOLA_NAVIGATION_SEARCH (gtk_type_new (hyperbola_navigation_search_get_type()));
}
