#include "config.h"
#include <gnome.h>
#include "hyperbola-types.h"
#include "hyperbola-nav-location.h"

static void hyperbola_navigation_location_class_init (HyperbolaNavigationLocationClass *klass);
static void hyperbola_navigation_location_init (HyperbolaNavigationLocation *view);
static void hyperbola_navigation_location_notify_location_change (HyperbolaView *view,
								  const HyperbolaLocationInfo *loci,
								  HyperbolaView *content_view,
								  HyperbolaNavigationContext *nav_context);
static void hyperbola_navigation_location_activate (GtkEntry * entry, HyperbolaNavigationLocation *view);

GtkType
hyperbola_navigation_location_get_type(void)
{
  static guint view_type = 0;

  if (!view_type)
    {
      GtkTypeInfo view_info = {
	"HyperbolaNavigationLocation",
	sizeof(HyperbolaNavigationLocation),
	sizeof(HyperbolaNavigationLocationClass),
	(GtkClassInitFunc) hyperbola_navigation_location_class_init,
	(GtkObjectInitFunc) hyperbola_navigation_location_init
      };

      view_type = gtk_type_unique (hyperbola_navigation_view_get_type(), &view_info);
    }

  return view_type;
}

static void
hyperbola_navigation_location_class_init (HyperbolaNavigationLocationClass *klass)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;
  HyperbolaViewClass *view_class;

  object_class = (GtkObjectClass*) klass;
  widget_class = (GtkWidgetClass*) klass;
  view_class = (HyperbolaViewClass *) klass;
  view_class->notify_location_change = hyperbola_navigation_location_notify_location_change;

  klass->parent_class = gtk_type_class (gtk_type_parent (object_class->type));
  ((HyperbolaNavigationViewClass *)klass)->nav_description = N_("Location");
}

static void
hyperbola_navigation_location_init (HyperbolaNavigationLocation *view)
{
  view->entry = gtk_entry_new();
  gtk_widget_show(view->entry);

  gtk_signal_connect(GTK_OBJECT(view->entry), "activate", GTK_SIGNAL_FUNC(hyperbola_navigation_location_activate), view);

  gtk_container_add(GTK_CONTAINER(view), view->entry);
}

HyperbolaNavigationLocation *
hyperbola_navigation_location_new(void)
{
  return HYPERBOLA_NAVIGATION_LOCATION (gtk_type_new (hyperbola_navigation_location_get_type()));
}

static void
hyperbola_navigation_location_activate (GtkEntry * entry, HyperbolaNavigationLocation *view)
{
  hyperbola_view_request_location_change(HYPERBOLA_VIEW(view), gtk_entry_get_text(GTK_ENTRY(view->entry)), NULL);
}

static void
hyperbola_navigation_location_notify_location_change (HyperbolaView *view,
						      const HyperbolaLocationInfo *loci,
						      HyperbolaView *content_view,
						      HyperbolaNavigationContext *nav_context)
{
  if(nav_context->initiating_view == ((GtkWidget *)view))
    return;

  gtk_entry_set_text(GTK_ENTRY(HYPERBOLA_NAVIGATION_LOCATION(view)->entry), loci->uri);
}
