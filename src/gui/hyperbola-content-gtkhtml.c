#include "hyperbola-content-gtkhtml.h"
#include <gtk/gtk.h>
#include <gtkhtml/gtkhtml.h>

static void hyperbola_content_gtkhtml_class_init (HyperbolaContentGtkHTMLClass *klass);
static void hyperbola_content_gtkhtml_init (HyperbolaContentGtkHTML *view);
static void hyperbola_content_gtkhtml_notify_location_change (HyperbolaView *view,
							      const HyperbolaLocationInfo *loci,
							      HyperbolaView *content_view,
							      HyperbolaNavigationContext *nav_context);
static void hyperbola_content_gtkhtml_url_requested(GtkHTML *htmlw, const char *url,
						    GtkHTMLStreamHandle handle, HyperbolaContentGtkHTML *view);

GtkType
hyperbola_content_gtkhtml_get_type(void)
{
  static guint view_type = 0;

  if (!view_type)
    {
      GtkTypeInfo view_info = {
	"HyperbolaContentGtkHTML",
	sizeof(HyperbolaContentGtkHTML),
	sizeof(HyperbolaContentGtkHTMLClass),
	(GtkClassInitFunc) hyperbola_content_gtkhtml_class_init,
	(GtkObjectInitFunc) hyperbola_content_gtkhtml_init
      };

      view_type = gtk_type_unique (hyperbola_view_get_type(), &view_info);
    }

  return view_type;
}

static void
hyperbola_content_gtkhtml_class_init (HyperbolaContentGtkHTMLClass *klass)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;
  HyperbolaViewClass *view_class;

  object_class = (GtkObjectClass*) klass;
  widget_class = (GtkWidgetClass*) klass;
  view_class = (HyperbolaViewClass*) klass;

  view_class->notify_location_change =  hyperbola_content_gtkhtml_notify_location_change;

  klass->parent_class = gtk_type_class (gtk_type_parent (object_class->type));
}

static void
hyperbola_content_gtkhtml_init (HyperbolaContentGtkHTML *view)
{
  GtkAdjustment *hadj, *vadj;

  view->scrollwin = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(view->scrollwin), GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
  hadj = gtk_scrolled_window_get_hadjustment(GTK_SCROLLED_WINDOW(view->scrollwin));
  vadj = gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(view->scrollwin));
  gtk_widget_push_colormap(gdk_rgb_get_cmap());
  gtk_widget_push_visual(gdk_rgb_get_visual());
  view->htmlw = gtk_html_new(hadj, vadj);
  gtk_widget_pop_visual();
  gtk_widget_pop_colormap();

  gtk_signal_connect(GTK_OBJECT(view->htmlw), "url_requested", hyperbola_content_gtkhtml_url_requested, view);
  gtk_widget_show(view->htmlw);
  gtk_widget_show(view->scrollwin);
  gtk_container_add(GTK_CONTAINER(view->scrollwin), view->htmlw);
  gtk_container_add(GTK_CONTAINER(view), view->scrollwin);
}

HyperbolaContentGtkHTML *
hyperbola_content_gtkhtml_new(void)
{
  return HYPERBOLA_CONTENT_GTKHTML (gtk_type_new (hyperbola_content_gtkhtml_get_type()));
}

static void
hyperbola_content_gtkhtml_notify_location_change (HyperbolaView *view,
						  const HyperbolaLocationInfo *loci,
						  HyperbolaView *content_view,
						  HyperbolaNavigationContext *nav_context)
{
  HyperbolaContentGtkHTML *hview;

  hview = HYPERBOLA_CONTENT_GTKHTML(view);

  g_assert(content_view == view);

  g_return_if_fail(loci->html_content);

  g_free(hview->cur_base);
  g_free(hview->cur_baseref);
  hview->cur_baseref = NULL;

  hview->cur_base = g_dirname(loci->filename);
  hview->html_content = loci->html_content;

  gtk_html_begin(GTK_HTML(hview->htmlw), NULL);
  gtk_html_parse(GTK_HTML(hview->htmlw));

  hview->html_content = NULL;
}

static void
hyperbola_content_gtkhtml_url_requested(GtkHTML *htmlw, const char *url,
					GtkHTMLStreamHandle handle, HyperbolaContentGtkHTML *view)
{
  char abuf[8192];
  int nread;
  FILE *fh;

  if(url)
    {
      char tmpurl[1024];
      const char *theurl;

      if(url[0] == '/')
	theurl = url;
      else
	{
	  theurl = tmpurl;
	  g_snprintf(tmpurl, sizeof(tmpurl), "%s/%s", view->cur_base, url);
	}

      fh = fopen(theurl, "r");
    }
  else
    fh = view->html_content;

  if(fh)
    {
      while((nread = fread(abuf, 1, sizeof(abuf), fh)) > 0)
	gtk_html_write(GTK_HTML(view->htmlw), handle, abuf, nread);

      gtk_html_end(GTK_HTML(view->htmlw), handle, GTK_HTML_STREAM_OK);

      if(url)
	fclose(fh);
    }
  else
    gtk_html_end(GTK_HTML(view->htmlw), handle, GTK_HTML_STREAM_ERROR);
}
