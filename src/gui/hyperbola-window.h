#ifndef HYPERBOLA_WINDOW_H
#define HYPERBOLA_WINDOW_H 1

#include <libgnomeui/gnome-app.h>
#include "hyperbola-types.h"
#include "hyperbola-view.h"

#define HYPERBOLA_TYPE_WINDOW (hyperbola_window_get_type())
#define HYPERBOLA_WINDOW(obj)	        (GTK_CHECK_CAST ((obj), HYPERBOLA_TYPE_WINDOW, HyperbolaWindow))
#define HYPERBOLA_WINDOW_CLASS(klass)      (GTK_CHECK_CLASS_CAST ((klass), HYPERBOLA_TYPE_WINDOW, HyperbolaWindowClass))
#define HYPERBOLA_IS_WINDOW(obj)	        (GTK_CHECK_TYPE ((obj), HYPERBOLA_TYPE_WINDOW))
#define HYPERBOLA_IS_WINDOW_CLASS(klass)   (GTK_CHECK_CLASS_TYPE ((klass), HYPERBOLA_TYPE_WINDOW))

typedef struct _HyperbolaWindow HyperbolaWindow;

typedef struct {
  GnomeAppClass parent_spot;

  GnomeAppClass *parent_class;

  void (* request_location_change)(HyperbolaWindow *window,
				   HyperbolaLocationReference loc,
				   HyperbolaNavigationContext *nav_context);
  guint window_signals[1];
} HyperbolaWindowClass;

struct _HyperbolaWindow {
  GnomeApp parent_object;

  HyperbolaView *content_view;

  GSList *nav_views;
  GtkWidget *nav_notebook, *content_hbox;
};

GtkType hyperbola_window_get_type(void);
GtkWidget *hyperbola_window_new(const char *app_id);
void hyperbola_window_set_content_view(HyperbolaWindow *window, HyperbolaView *content_view);
void hyperbola_window_add_navigation_view(HyperbolaWindow *window, HyperbolaView *nav_view);
void hyperbola_window_remove_navigation_view(HyperbolaWindow *window, HyperbolaView *nav_view);
void hyperbola_window_request_location_change(HyperbolaWindow *window,
					      HyperbolaLocationReference loc,
					      HyperbolaNavigationContext *nav_context);
void hyperbola_window_save_state(HyperbolaWindow *window, const char *config_path);
void hyperbola_window_load_state(HyperbolaWindow *window, const char *config_path);
void hyperbola_window_set_default_state(HyperbolaWindow *window);

#endif
