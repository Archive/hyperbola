#ifndef HYPERBOLA_FILEFMT_H
#define HYPERBOLA_FILEFMT_H 1

#include "hyperbola-types.h"

/* Fills in an already-allocated HyperbolaLocationInfo structure */
HyperbolaLocationInfo *hyperbola_location_info_init(HyperbolaLocationInfo *loci, const HyperbolaLocationReference loc);

/* Frees contents of 'loci', does not free 'loci' */
void hyperbola_location_info_destroy(HyperbolaLocationInfo *loci);

void hyperbola_doc_tree_populate(HyperbolaDocTree *tree);
HyperbolaDocTree * hyperbola_doc_tree_new(void);
void hyperbola_doc_tree_destroy(HyperbolaDocTree *tree);
void hyperbola_doc_tree_add(HyperbolaDocTree *tree, HyperbolaTreeNodeType type, const char **path,
			    const char *title, const char *uri);

#endif
