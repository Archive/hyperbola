#ifndef HYPERBOLA_CONTENT_VIEW_H
#define HYPERBOLA_CONTENT_VIEW_H 1

#include "hyperbola-view.h"

#define HYPERBOLA_TYPE_CONTENT_VIEW (hyperbola_content_view_get_type())
#define HYPERBOLA_CONTENT_VIEW(obj)	        (GTK_CHECK_CAST ((obj), HYPERBOLA_TYPE_CONTENT_VIEW, HyperbolaContentView))
#define HYPERBOLA_CONTENT_VIEW_CLASS(klass)      (GTK_CHECK_CLASS_CAST ((klass), HYPERBOLA_TYPE_CONTENT_VIEW, HyperbolaContentViewClass))
#define HYPERBOLA_IS_CONTENT_VIEW(obj)	        (GTK_CHECK_TYPE ((obj), HYPERBOLA_TYPE_CONTENT_VIEW))
#define HYPERBOLA_IS_CONTENT_VIEW_CLASS(klass)   (GTK_CHECK_CLASS_TYPE ((klass), HYPERBOLA_TYPE_CONTENT_VIEW))

typedef struct {
  HyperbolaViewClass parent_spot;

  HyperbolaViewClass *parent_class;
} HyperbolaContentViewClass;

typedef struct {
  HyperbolaView parent_object;
} HyperbolaContentView;

GtkType hyperbola_content_view_get_type(void);
HyperbolaContentView *hyperbola_content_view_new(void);

#endif
