#ifndef HYPERBOLA_NAV_SEARCH_H
#define HYPERBOLA_NAV_SEARCH_H 1

#include "hyperbola-nav-view.h"

#define HYPERBOLA_TYPE_NAVIGATION_SEARCH (hyperbola_navigation_search_get_type())
#define HYPERBOLA_NAVIGATION_SEARCH(obj)	        (GTK_CHECK_CAST ((obj), HYPERBOLA_TYPE_NAVIGATION_SEARCH, HyperbolaNavigationSearch))
#define HYPERBOLA_NAVIGATION_SEARCH_CLASS(klass)      (GTK_CHECK_CLASS_CAST ((klass), HYPERBOLA_TYPE_NAVIGATION_SEARCH, HyperbolaNavigationSearchClass))
#define HYPERBOLA_IS_NAVIGATION_SEARCH(obj)	        (GTK_CHECK_TYPE ((obj), HYPERBOLA_TYPE_NAVIGATION_SEARCH))
#define HYPERBOLA_IS_NAVIGATION_SEARCH_CLASS(klass)   (GTK_CHECK_CLASS_TYPE ((klass), HYPERBOLA_TYPE_NAVIGATION_SEARCH))

typedef struct _HyperbolaNavigationSearch HyperbolaNavigationSearch;

typedef struct {
  HyperbolaNavigationViewClass parent_spot;

  HyperbolaNavigationViewClass *parent_class;
} HyperbolaNavigationSearchClass;

struct _HyperbolaNavigationSearch {
  HyperbolaNavigationView parent_object;
};

GtkType hyperbola_navigation_search_get_type(void);
HyperbolaNavigationSearch *hyperbola_navigation_search_new(void);

#endif
