#ifndef HYPERBOLA_NAV_VIEW_H
#define HYPERBOLA_NAV_VIEW_H 1

#include "hyperbola-view.h"

#define HYPERBOLA_TYPE_NAVIGATION_VIEW (hyperbola_navigation_view_get_type())
#define HYPERBOLA_NAVIGATION_VIEW(obj)	        (GTK_CHECK_CAST ((obj), HYPERBOLA_TYPE_NAVIGATION_VIEW, HyperbolaNavigationView))
#define HYPERBOLA_NAVIGATION_VIEW_CLASS(klass)      (GTK_CHECK_CLASS_CAST ((klass), HYPERBOLA_TYPE_NAVIGATION_VIEW, HyperbolaNavigationViewClass))
#define HYPERBOLA_IS_NAVIGATION_VIEW(obj)	        (GTK_CHECK_TYPE ((obj), HYPERBOLA_TYPE_NAVIGATION_VIEW))
#define HYPERBOLA_IS_NAVIGATION_VIEW_CLASS(klass)   (GTK_CHECK_CLASS_TYPE ((klass), HYPERBOLA_TYPE_NAVIGATION_VIEW))

typedef struct _HyperbolaNavigationView HyperbolaNavigationView;

typedef struct {
  HyperbolaViewClass parent_spot;

  HyperbolaViewClass *parent_class;
  char *nav_description;
} HyperbolaNavigationViewClass;

struct _HyperbolaNavigationView {
  HyperbolaView parent_object;
};

GtkType hyperbola_navigation_view_get_type(void);
HyperbolaNavigationView *hyperbola_navigation_view_new(void);
const char *hyperbola_navigation_view_get_description(HyperbolaNavigationView *nview);

#endif
