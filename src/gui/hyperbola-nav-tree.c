#include "hyperbola-nav-tree.h"
#include "hyperbola-filefmt.h"
#include <gtk/gtk.h>

static void hyperbola_navigation_tree_class_init (HyperbolaNavigationTreeClass *klass);
static void hyperbola_navigation_tree_init (HyperbolaNavigationTree *view);
static void hyperbola_navigation_tree_select_row(GtkCTree *ctree, GtkCTreeNode *node,
						 gint column, HyperbolaNavigationTree *view);
static void hyperbola_navigation_tree_notify_location_change (HyperbolaView *view,
							      const HyperbolaLocationInfo *loci,
							      HyperbolaView *content_view,
							      HyperbolaNavigationContext *nav_context);

GtkType
hyperbola_navigation_tree_get_type(void)
{
  static guint view_type = 0;

  if (!view_type)
    {
      GtkTypeInfo view_info = {
	"HyperbolaNavigationTree",
	sizeof(HyperbolaNavigationTree),
	sizeof(HyperbolaNavigationTreeClass),
	(GtkClassInitFunc) hyperbola_navigation_tree_class_init,
	(GtkObjectInitFunc) hyperbola_navigation_tree_init
      };

      view_type = gtk_type_unique (hyperbola_navigation_view_get_type(), &view_info);
    }

  return view_type;
}

static void
hyperbola_navigation_tree_class_init (HyperbolaNavigationTreeClass *klass)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;
  HyperbolaViewClass *view_class;

  object_class = (GtkObjectClass*) klass;
  widget_class = (GtkWidgetClass*) klass;
  view_class = (HyperbolaViewClass *) klass;

  view_class->notify_location_change = hyperbola_navigation_tree_notify_location_change;

  klass->parent_class = gtk_type_class (gtk_type_parent (object_class->type));

  ((HyperbolaNavigationViewClass *)klass)->nav_description = N_("Contents");
}

typedef struct {
  HyperbolaNavigationTree *view;
  GtkCTreeNode *sibling, *parent;
} PopulateInfo;

static gboolean
ctree_populate_subnode(gpointer key, gpointer value, gpointer user_data)
{
  HyperbolaTreeNode *node = value;
  PopulateInfo *pi = (PopulateInfo *)user_data, subpi;
  gboolean term;

  term = (node->type == HYP_TREE_NODE_PAGE) || !node->children;
  pi->sibling = gtk_ctree_insert_node(GTK_CTREE(pi->view->ctree), pi->parent, NULL, &node->title, 5,
				      NULL, NULL, NULL, NULL, term, FALSE);
  node->user_data = pi->sibling;

  gtk_ctree_node_set_row_data(GTK_CTREE(pi->view->ctree), pi->sibling, node);

  if(node->children)
    {
      subpi.view = pi->view;
      subpi.sibling = NULL;
      subpi.parent = pi->sibling;
      g_tree_traverse(node->children, ctree_populate_subnode, G_IN_ORDER, &subpi);
    }

  return FALSE;
}

static void
ctree_populate(HyperbolaNavigationTree *view)
{
  PopulateInfo subpi = {NULL};

  subpi.view = view;

  g_tree_traverse(view->doc_tree->children, ctree_populate_subnode, G_IN_ORDER, &subpi);
}

static void
hyperbola_navigation_tree_init (HyperbolaNavigationTree *view)
{
  static const char *titles[] = {"Document Tree"};
  GtkWidget *wtmp;

  view->ctree = gtk_ctree_new_with_titles(1, 0, (gchar **)titles);
  gtk_clist_freeze(GTK_CLIST(view->ctree));
  gtk_clist_set_selection_mode(GTK_CLIST(view->ctree), GTK_SELECTION_BROWSE);
  gtk_signal_connect(GTK_OBJECT(view->ctree), "tree_select_row", hyperbola_navigation_tree_select_row, view);
  view->doc_tree = hyperbola_doc_tree_new();
  hyperbola_doc_tree_populate(view->doc_tree);

  ctree_populate(view);

  wtmp = gtk_scrolled_window_new(gtk_clist_get_hadjustment(GTK_CLIST(view->ctree)),
				 gtk_clist_get_vadjustment(GTK_CLIST(view->ctree)));
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(wtmp), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_container_add(GTK_CONTAINER(view), wtmp);

  gtk_container_add(GTK_CONTAINER(wtmp), view->ctree);
  gtk_clist_columns_autosize(GTK_CLIST(view->ctree));
  gtk_clist_thaw(GTK_CLIST(view->ctree));
  gtk_widget_show(view->ctree);
  gtk_widget_show(wtmp);
}

HyperbolaNavigationTree *
hyperbola_navigation_tree_new(void)
{
  return HYPERBOLA_NAVIGATION_TREE (gtk_type_new (hyperbola_navigation_tree_get_type()));
}

static void
hyperbola_navigation_tree_notify_location_change (HyperbolaView *view,
						  const HyperbolaLocationInfo *loci,
						  HyperbolaView *content_view,
						  HyperbolaNavigationContext *nav_context)
{
  HyperbolaNavigationTree *hview = (HyperbolaNavigationTree *)view;
  HyperbolaTreeNode *tnode;

  if(hview->notify_count > 0)
    return;

  hview->notify_count++;

  tnode = g_hash_table_lookup(hview->doc_tree->global_by_uri, loci->uri);

  if(tnode)
    gtk_ctree_select(GTK_CTREE(hview->ctree), tnode->user_data);

  hview->notify_count--;
}

static void hyperbola_navigation_tree_select_row(GtkCTree *ctree, GtkCTreeNode *node,
						 gint column, HyperbolaNavigationTree *view)
{
  HyperbolaTreeNode *tnode;

  if(view->notify_count > 0)
    return;

  tnode = gtk_ctree_node_get_row_data(ctree, node);

  if(!tnode || !tnode->uri)
    return;

  view->notify_count++;

  hyperbola_view_request_location_change(HYPERBOLA_VIEW(view), tnode->uri, NULL);

  view->notify_count--;
}

