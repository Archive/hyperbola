#include "config.h"
#include <gnome.h>
#include "hyperbola.h"

static void hyperbola_window_class_init (HyperbolaWindowClass *klass);
static void hyperbola_window_init (HyperbolaWindow *window);
static void hyperbola_window_set_arg (GtkObject      *object,
				      GtkArg         *arg,
				      guint	      arg_id);
static void hyperbola_window_get_arg (GtkObject      *object,
				      GtkArg         *arg,
				      guint	      arg_id);
static void hyperbola_window_close (GtkWidget *widget,
				    GtkWidget *window);
static void hyperbola_window_real_request_location_change (HyperbolaWindow *window,
							   HyperbolaLocationReference loc,
							   HyperbolaNavigationContext *nav_context);

GtkType
hyperbola_window_get_type(void)
{
  static guint window_type = 0;

  if (!window_type)
    {
      GtkTypeInfo window_info = {
	"HyperbolaWindow",
	sizeof(HyperbolaWindow),
	sizeof(HyperbolaWindowClass),
	(GtkClassInitFunc) hyperbola_window_class_init,
	(GtkObjectInitFunc) hyperbola_window_init
      };

      window_type = gtk_type_unique (gnome_app_get_type(), &window_info);
    }

  return window_type;
}

typedef void (*GtkSignal_NONE__BOXED_BOXED) (GtkObject * object,
					     gpointer arg1,
					     gpointer arg2,
					     gpointer user_data);
static void 
gtk_marshal_NONE__BOXED_BOXED (GtkObject * object,
			       GtkSignalFunc func,
			       gpointer func_data,
			       GtkArg * args)
{
  GtkSignal_NONE__BOXED_BOXED rfunc;
  rfunc = (GtkSignal_NONE__BOXED_BOXED) func;
  (*rfunc) (object,
	    GTK_VALUE_BOXED (args[0]),
	    GTK_VALUE_BOXED (args[1]),
	    func_data);
}

enum {
  ARG_0,
  ARG_APP_ID,
  ARG_CONTENT_VIEW
};

static void
hyperbola_window_class_init (HyperbolaWindowClass *klass)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;
  int i;

  object_class = (GtkObjectClass*) klass;
  object_class->get_arg = hyperbola_window_get_arg;
  object_class->set_arg = hyperbola_window_set_arg;

  widget_class = (GtkWidgetClass*) klass;
  klass->parent_class = gtk_type_class (gtk_type_parent (object_class->type));

  klass->request_location_change = hyperbola_window_real_request_location_change;

  i = 0;
  klass->window_signals[i++] = gtk_signal_new("request_location_change",
					      GTK_RUN_LAST,
					      object_class->type,
					      GTK_SIGNAL_OFFSET (HyperbolaWindowClass, request_location_change),
					      gtk_marshal_NONE__BOXED_BOXED,
					      GTK_TYPE_NONE, 2, GTK_TYPE_BOXED, GTK_TYPE_BOXED);
  gtk_object_class_add_signals (object_class, klass->window_signals, i);

  gtk_object_add_arg_type ("HyperbolaWindow::app_id",
			   GTK_TYPE_STRING,
			   GTK_ARG_READWRITE|GTK_ARG_CONSTRUCT,
			   ARG_APP_ID);
  gtk_object_add_arg_type ("HyperbolaWindow::content_view",
			   GTK_TYPE_OBJECT,
			   GTK_ARG_READWRITE,
			   ARG_CONTENT_VIEW);
}

static void
hw_view_add(HyperbolaWindow *window, GtkType vtype)
{
  GtkWidget *locview;

  locview = gtk_widget_new(vtype, "main_window", window, NULL);
  gtk_widget_show(locview);

  hyperbola_window_add_navigation_view(window, HYPERBOLA_VIEW(locview));
}

static void
hw_history_add(GtkWidget *widget, HyperbolaWindow *window)
{
  hw_view_add(window, hyperbola_navigation_history_get_type());
}

static void
hw_location_add(GtkWidget *widget, HyperbolaWindow *window)
{
  hw_view_add(window, hyperbola_navigation_location_get_type());
}

static void
hw_search_add(GtkWidget *widget, HyperbolaWindow *window)
{
  hw_view_add(window, hyperbola_navigation_search_get_type());
}

static void
hw_tree_add(GtkWidget *widget, HyperbolaWindow *window)
{
  hw_view_add(window, hyperbola_navigation_tree_get_type());
}

static void
hyperbola_window_constructed(HyperbolaWindow *window)
{
  GnomeApp *app;
  GnomeUIInfo new_items_menu[] = {
    GNOMEUIINFO_ITEM("History", "Add a new history navigator", hw_history_add, NULL),
    GNOMEUIINFO_ITEM("Location", "Add a new location navigator", hw_location_add, NULL),
    GNOMEUIINFO_ITEM("Search", "Add a new search navigator", hw_search_add, NULL),
    GNOMEUIINFO_ITEM("Tree", "Add a new tree navigator", hw_tree_add, NULL),
    GNOMEUIINFO_END
  };
  GnomeUIInfo file_menu[] = {
    GNOMEUIINFO_MENU_CLOSE_ITEM(hyperbola_window_close, NULL),
    GNOMEUIINFO_MENU_NEW_SUBTREE(new_items_menu),
    GNOMEUIINFO_END
  };
  GnomeUIInfo main_menu[] = {
    GNOMEUIINFO_MENU_VIEW_TREE(file_menu),
    GNOMEUIINFO_END
  };

  app = GNOME_APP(window);

  gnome_app_create_menus_with_data(app, main_menu, window);
  gnome_app_set_statusbar(app, gtk_statusbar_new());
  gnome_app_install_menu_hints(app, main_menu);

  gtk_window_set_policy(GTK_WINDOW(window), FALSE, TRUE, FALSE);

  window->content_hbox = gtk_hpaned_new();
  gtk_widget_show(window->content_hbox);
  gnome_app_set_contents(app, window->content_hbox);

  window->nav_notebook = gtk_notebook_new();
  gtk_widget_show(window->nav_notebook);
#if 0
  gnome_app_add_docked(app, window->nav_notebook, "Nav_Notebook",
		       GNOME_DOCK_ITEM_BEH_EXCLUSIVE|GNOME_DOCK_ITEM_BEH_NEVER_FLOATING|GNOME_DOCK_ITEM_BEH_LOCKED,
		       GNOME_DOCK_RIGHT, 0, 0, 0);
#elif 0
  gtk_box_pack_end(GTK_BOX(window->content_hbox), window->nav_notebook, FALSE, FALSE, GNOME_PAD);
#else
  gtk_paned_pack2(GTK_PANED(window->content_hbox), window->nav_notebook, TRUE, TRUE);
#endif
}

static void
hyperbola_window_set_arg (GtkObject      *object,
			  GtkArg         *arg,
			  guint	      arg_id)
{
  GnomeApp *app = (GnomeApp *) object;
  char *old_app_name;
  GtkWidget *new_cv;
  HyperbolaWindow *window = (HyperbolaWindow *) object;

  switch(arg_id) {
  case ARG_APP_ID:
    if(!GTK_VALUE_STRING(*arg))
      return;

    old_app_name = app->name;
    g_free(app->name);
    app->name = g_strdup(GTK_VALUE_STRING(*arg));
    g_assert(app->name);
    g_free(app->prefix);
    app->prefix = g_strconcat("/", app->name, "/", NULL);
    if(!old_app_name)
      hyperbola_window_constructed(HYPERBOLA_WINDOW(object));
    break;
  case ARG_CONTENT_VIEW:
    new_cv = GTK_WIDGET(GTK_VALUE_OBJECT(*arg));
    if(window->content_view)
      {
	gtk_widget_ref(GTK_WIDGET(window->content_view));
	gtk_container_remove(GTK_CONTAINER(window->content_hbox), GTK_WIDGET(window->content_view));
#if 0
	gnome_app_set_contents(app, new_cv);
#elif 0
	gtk_box_pack_start(GTK_BOX(window->content_hbox), new_cv, TRUE, TRUE, GNOME_PAD);
#else
	gtk_paned_pack1(GTK_PANED(window->content_hbox), new_cv, TRUE, FALSE);
#endif
	gtk_widget_unref(GTK_WIDGET(window->content_view));
      }
    else
#if 0
      gnome_app_set_contents(app, new_cv);
#elif 0
      gtk_box_pack_start(GTK_BOX(window->content_hbox), new_cv, TRUE, TRUE, GNOME_PAD);
#else
      gtk_paned_pack1(GTK_PANED(window->content_hbox), new_cv, TRUE, FALSE);
#endif
      gtk_widget_queue_resize(window->content_hbox);
    window->content_view = HYPERBOLA_VIEW(new_cv);
    break;
  }
}

static void
hyperbola_window_get_arg (GtkObject      *object,
			  GtkArg         *arg,
			  guint	      arg_id)
{
  GnomeApp *app = (GnomeApp *) object;

  switch(arg_id) {
  case ARG_APP_ID:
    GTK_VALUE_STRING(*arg) = app->name;
    break;
  case ARG_CONTENT_VIEW:
    GTK_VALUE_OBJECT(*arg) = GTK_OBJECT(((HyperbolaWindow *)object)->content_view);
    break;
  }
}

static void
hyperbola_window_init (HyperbolaWindow *window)
{
}

GtkWidget *
hyperbola_window_new(const char *app_id)
{
  return GTK_WIDGET (gtk_object_new (hyperbola_window_get_type(), "app_id", app_id, NULL));
}

static void
hyperbola_window_close (GtkWidget *widget,
			GtkWidget *window)
{
  gtk_widget_destroy(window);
}

void
hyperbola_window_set_content_view(HyperbolaWindow *window, HyperbolaView *content_view)
{
  gtk_object_set(GTK_OBJECT(window), "content_view", content_view, NULL);
  gtk_widget_show(GTK_WIDGET(content_view));
}

#if 0
static gboolean
hyperbola_window_send_show_properties(GtkWidget *dockitem, GdkEventButton *event, HyperbolaView *nav_view)
{
  if(event->button != 3)
    return FALSE;

  gtk_signal_emit_stop_by_name(GTK_OBJECT(dockitem), "button_press_event");

  gtk_signal_emit_by_name(GTK_OBJECT(nav_view), "show_properties");

  return TRUE;
}
#endif

void
hyperbola_window_add_navigation_view(HyperbolaWindow *window, HyperbolaView *nav_view)
{
  GtkWidget *label;

  g_return_if_fail(!g_slist_find(window->nav_views, nav_view));
  g_return_if_fail(HYPERBOLA_IS_NAVIGATION_VIEW(nav_view));

  label = gtk_label_new(hyperbola_navigation_view_get_description(HYPERBOLA_NAVIGATION_VIEW(nav_view)));
  gtk_widget_show(label);
  gtk_notebook_prepend_page(GTK_NOTEBOOK(window->nav_notebook), GTK_WIDGET(nav_view), label);
  gtk_widget_show(GTK_WIDGET(nav_view));

  window->nav_views = g_slist_prepend(window->nav_views, nav_view);
}

void
hyperbola_window_remove_navigation_view(HyperbolaWindow *window, HyperbolaView *nav_view)
{
  gint pagenum;

  g_return_if_fail(g_slist_find(window->nav_views, nav_view));

  window->nav_views = g_slist_remove(window->nav_views, nav_view);

  pagenum = gtk_notebook_page_num(GTK_NOTEBOOK(window->nav_notebook), GTK_WIDGET(nav_view));

  g_return_if_fail(pagenum >= 0);

  gtk_notebook_remove_page(GTK_NOTEBOOK(window->nav_notebook), pagenum);
}

void
hyperbola_window_request_location_change(HyperbolaWindow *window,
					 HyperbolaLocationReference loc,
					 HyperbolaNavigationContext *nav_context)
{
  HyperbolaWindowClass *klass;
  GtkObject *obj;

  obj = GTK_OBJECT(window);

  klass = HYPERBOLA_WINDOW_CLASS(obj->klass);
  gtk_signal_emit(obj, klass->window_signals[0], loc, nav_context);
}

static void
hyperbola_window_real_request_location_change (HyperbolaWindow *window,
					       HyperbolaLocationReference loc,
					       HyperbolaNavigationContext *nav_context)
{
  guint signum;
  GSList *cur;
  HyperbolaLocationInfo loci_spot, *loci;

  loci = hyperbola_location_info_init(&loci_spot, loc);

  if(!loci)
    {
      gnome_url_show(loc);
      return;
    }

  signum = gtk_signal_lookup("notify_location_change", hyperbola_view_get_type());

  for(cur = window->nav_views; cur; cur = cur->next)
    gtk_signal_emit(GTK_OBJECT(cur->data), signum, loci, window->content_view, nav_context);

  gtk_signal_emit(GTK_OBJECT(window->content_view), signum, loci, window->content_view, nav_context);

  hyperbola_location_info_destroy(loci);
}

void
hyperbola_window_save_state(HyperbolaWindow *window, const char *config_path)
{
  GSList *cur;
  int n;
  guint signum;
  char cbuf[1024];

  gnome_config_push_prefix(config_path);
  if(window->content_view)
    {
      gnome_config_set_string("content_view_type", gtk_type_name(GTK_OBJECT_TYPE(window->content_view)));
      g_snprintf(cbuf, sizeof(cbuf), "%s/Content_View/", config_path);

      gnome_config_push_prefix(cbuf);

      gtk_signal_emit(GTK_OBJECT(window->content_view), signum, cbuf);

      gnome_config_pop_prefix();
    }
  else
    gnome_config_set_string("content_view_type", "NONE");


  n = g_slist_length(window->nav_views);
  gnome_config_set_int("num_nav_views", n);
  signum = gtk_signal_lookup("save_state", hyperbola_view_get_type());
  for(n = 0, cur = window->nav_views; cur; cur = cur->next, n++)
    {
      g_snprintf(cbuf, sizeof(cbuf), "nav_view_%d_type=0", n);

      gnome_config_set_string(cbuf, gtk_type_name(GTK_OBJECT_TYPE(cur->data)));

      g_snprintf(cbuf, sizeof(cbuf), "%s/Nav_View_%d/", config_path, n);

      gnome_config_push_prefix(cbuf);

      gtk_signal_emit(GTK_OBJECT(cur->data), signum, cbuf);

      gnome_config_pop_prefix();
    }

  gnome_config_pop_prefix();
}

void
hyperbola_window_set_default_state(HyperbolaWindow *window)
{
  GSList *cur;
  GtkRequisition sreq;
  GdkGeometry geo;

  /* Remove old stuff */
  for(cur = window->nav_views; cur; cur = cur->next)
    hyperbola_window_remove_navigation_view(window, HYPERBOLA_VIEW(cur->data));

  hyperbola_window_set_content_view(window,
				    HYPERBOLA_VIEW(gtk_widget_new(hyperbola_content_gtkhtml_get_type(),
								  "main_window", window, NULL))
				    );

  hyperbola_window_add_navigation_view(window, HYPERBOLA_VIEW(gtk_widget_new(hyperbola_navigation_location_get_type(),
									     "main_window", window, NULL))
				       );
  hyperbola_window_add_navigation_view(window, HYPERBOLA_VIEW(gtk_widget_new(hyperbola_navigation_history_get_type(),
									     "main_window", window, NULL))
				       );

  hyperbola_window_add_navigation_view(window, HYPERBOLA_VIEW(gtk_widget_new(hyperbola_navigation_tree_get_type(),
									     "main_window", window, NULL))
				       );

  gtk_widget_size_request(GTK_WIDGET(window), &sreq);
  gtk_widget_size_request(GTK_WIDGET(window->content_hbox), &sreq);
  gtk_widget_size_request(GTK_WIDGET(window->nav_notebook), &sreq);
  geo.min_width = window->nav_notebook->requisition.width + MAX(400, GTK_WIDGET(window->content_view)->requisition.width);
  geo.min_height = window->nav_notebook->requisition.height + MAX(200, GTK_WIDGET(window->content_view)->requisition.height);
  geo.base_width = geo.min_width;
  geo.base_height = geo.min_height;
  gtk_window_set_geometry_hints(GTK_WINDOW(window), NULL, &geo,
				GDK_HINT_BASE_SIZE|GDK_HINT_MIN_SIZE);
  gtk_window_set_default_size(GTK_WINDOW(window), geo.min_width, geo.min_height);

  gtk_paned_set_position(GTK_PANED(window->content_hbox), MAX(400, GTK_WIDGET(window->content_view)->requisition.width));
}

void
hyperbola_window_load_state(HyperbolaWindow *window, const char *config_path)
{
  char *vtype;
  GSList *cur;
  int i, n;
  guint signum;
  char cbuf[1024];

  /* Remove old stuff */
  for(cur = window->nav_views; cur; cur = cur->next)
    hyperbola_window_remove_navigation_view(window, HYPERBOLA_VIEW(cur->data));
  hyperbola_window_set_content_view(window, NULL);

  /* Load new stuff */
  gnome_config_push_prefix(config_path);

  vtype = gnome_config_get_string("content_view_type=NONE");
  signum = gtk_signal_lookup("load_state", hyperbola_view_get_type());

  if(vtype && strcmp(vtype, "NONE")) /* Create new content view */
    {
      GtkWidget *w;
      GtkType wt;

      wt = gtk_type_from_name(vtype);
      g_assert(wt);
      g_assert(gtk_type_is_a(wt, HYPERBOLA_TYPE_VIEW));

      w = gtk_widget_new(wt, "main_window", window, NULL);
      hyperbola_window_set_content_view(window, HYPERBOLA_VIEW(w));

      g_snprintf(cbuf, sizeof(cbuf), "%s/Content_View/", config_path);
      gnome_config_push_prefix(cbuf);
      gtk_signal_emit(GTK_OBJECT(w), signum, cbuf);
      gnome_config_pop_prefix();

      gtk_widget_show(w);
    }
  g_free(vtype);

  n = gnome_config_get_int("num_nav_views=0");
  for(i = 0; i < n; n++)
    {
      GtkType nvt;
      GtkWidget *nvw;

      g_snprintf(cbuf, sizeof(cbuf), "%s/nav_view_%d_type=0", config_path, i);
      vtype = gnome_config_get_string(cbuf);
      nvt = gtk_type_from_name(vtype);
      g_free(vtype);
      g_assert(nvt);

      g_assert(gtk_type_is_a(nvt, HYPERBOLA_TYPE_VIEW));

      nvw = gtk_widget_new(nvt, "main_window", window, NULL);

      g_snprintf(cbuf, sizeof(cbuf), "%s/Nav_View_%d/", config_path, i);

      gnome_config_push_prefix(cbuf);
      gtk_signal_emit(GTK_OBJECT(cur->data), signum, cbuf);
      gnome_config_pop_prefix();

      hyperbola_window_add_navigation_view(window, HYPERBOLA_VIEW(nvw));
    }

  gnome_config_pop_prefix();
}
