#include "hyperbola-nav-view.h"
#include <gtk/gtksignal.h>

static void hyperbola_navigation_view_class_init (HyperbolaNavigationViewClass *klass);
static void hyperbola_navigation_view_init (HyperbolaNavigationView *view);

GtkType
hyperbola_navigation_view_get_type(void)
{
  static guint view_type = 0;

  if (!view_type)
    {
      GtkTypeInfo view_info = {
	"HyperbolaNavigationView",
	sizeof(HyperbolaNavigationView),
	sizeof(HyperbolaNavigationViewClass),
	(GtkClassInitFunc) hyperbola_navigation_view_class_init,
	(GtkObjectInitFunc) hyperbola_navigation_view_init
      };

      view_type = gtk_type_unique (hyperbola_view_get_type(), &view_info);
    }

  return view_type;
}

static void
hyperbola_navigation_view_class_init (HyperbolaNavigationViewClass *klass)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;

  object_class = (GtkObjectClass*) klass;
  widget_class = (GtkWidgetClass*) klass;
  klass->parent_class = gtk_type_class (gtk_type_parent (object_class->type));
}

static void
hyperbola_navigation_view_init (HyperbolaNavigationView *view)
{
}

HyperbolaNavigationView *
hyperbola_navigation_view_new(void)
{
  return HYPERBOLA_NAVIGATION_VIEW (gtk_type_new (hyperbola_navigation_view_get_type()));
}

const char *
hyperbola_navigation_view_get_description(HyperbolaNavigationView *nview)
{
  return _(HYPERBOLA_NAVIGATION_VIEW_CLASS(GTK_OBJECT(nview)->klass)->nav_description);
}
