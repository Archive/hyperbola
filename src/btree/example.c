/*
 * Copyright (C) 1988, Marcus J. Ranum, William Welch Medical Library
 * $Author$
 */

/*
 * $Log$
 * Revision 1.1  1999/11/12 15:47:05  sopwith
 * Initial revision
 *
 * Revision 1.1  88/06/01  21:35:15  mjr
 * Initial revision
 * 
 */


/* nobody ever writes nice test programs. in fact, this one is pretty gross */
/* basically, this allows exercising all the various functions of the btree */
/* library */

#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include "btree.h"

int
main(int ac, char *av[])
{
	BTREE	h1;
	int i = 0;
	
	BTNODENUM node_num;
	char *key;
	char	instr[BUFSIZ];
	BTRECNUM node_nbr;

	if(ac < 2) {
		(void)fprintf(stderr,"usage: %s <file>\n",av[0]);
		exit(1);
	}

	if((h1 = btopen(av[1], O_CREAT|O_RDWR, 0600, 64)) ==NULL) {
		btperror(av[1]);
		exit(1);
	}


	while (1) {
		(void)printf("Find  Next  Tail  Head  Prev  Insrt  Del  Quit:");
		if(gets(instr) == NULL)
			exit(btclose(h1));

		switch (*instr) {

		case 'f':
		case 'F':
			(void)printf("\nKey to Find: ");
			(void)gets(instr);
			(void)strncpy(instr, instr, BT_KSIZ - 1);
			instr[BT_KSIZ - 1] = '\0';
			switch(btfind(instr, &node_num, &key, &node_nbr, h1)) {
			case BT_NREC:
				(void)printf("not found: closest before=%s\n", key);
				break;
			case -1:
				btperror("find");
				break;
			default:
				(void)printf("current=%s\n", key);
				break;
			}
			break;

		case 'h':
		case 'H':
			switch(bthead(&node_num, &key, &node_nbr, h1)) {
			case (-1):
				btperror("bthead() returns -1");
				continue;
				break;
			default:
				(void)printf("current=%s\n", key);
			}
			break;

		case 't':
		case 'T':
			switch(bttail(&node_num, &key, &node_nbr, h1)) {
			case (-1):
				btperror("bttail() returns -1");
				continue;
				break;
			default:
				(void)printf("current=%s\n", key);
			}
			break;

		case 'd':
		case 'D':
			if(btdelete(node_num, h1) < 0) {
				btperror("delete failed");
			} else {
				(void)printf("...deleted\n");
			}
			break;

		case 'n':
		case 'N':
			switch (btnext(&node_num, &key, &node_nbr, h1)) {
			case (-1):
				btperror("btnext() returns -1");
				continue;
				break;

			case (0):
				(void)printf("current=%s\n", key);
				break;

			case (BT_EOF):
				(void)printf("end of file: current=%s\n",key);
				break;
			}
			break;

		case 'p':
		case 'P':
			switch (btprevious(&node_num, &key, &node_nbr, h1)) {
			case (-1):
				btperror("btprevious() returns -1");
				continue;
				break;
			case (0):
				(void)printf("current=%s\n", key);
				break;
			case (BT_EOF):
				(void)printf("start of file: current=%s\n",key);
			}
			break;

		case 'i':
		case 'I':
			(void)printf("Enter a key: ");
			(void)gets(instr);

			/* h1->sblk.free is used here as arbitrary record # */
			/* typical use would be to set this to the recno */
			/* for whatever it is that is being indexed into */
			if(btinsert(instr, i++, h1) < 0)
				btperror("insert failed");
			else
				(void)printf("...inserted\n");
			break;

		case 'q':
		case 'Q':
			exit(btclose(h1));

		default:
			(void)printf("huh?\n");
		}
	}
}
