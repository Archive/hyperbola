/*#define DEBUG*/
/*+-------------------------------------------------------------------------
	btree.c
	hacked by ...!gatech!kd4nc!n4hgf!wht

Copyright (C) 1988, Marcus J.  Ranum, William Welch Medical Library
$Author$ $Log$
$Author: sopwith $ Revision 1.2  1999/11/18 21:18:35  sopwith
$Author: sopwith $ Woohoo, add basic GUI object framework.
$Author: sopwith $
$Author$ Revision 1.1.1.1  1999/11/12 15:47:05  sopwith
$Author$
$Author$
$Author$ Initial import.
$Author$ Revision 1.1 88/06/01 21:35:07 mjr
Initial revision
The original code was placed in the public domain.  This is not, since I
wish to retain some control over it.  No restrictions are placed on
modification, or use of this code, as long as the copyrights are not
removed.  There are some areas that really could use improving (like
handling free nodes better) and I hope that if someone makes
improvements they will keep me up to date on them (e-mail please, I am
not on usenet).
Marcus J. Ranum, William Welch Medical Library, 1988
mjr@jhuigf.BITNET || uunet!mimsy!aplcen!osiris!welchvax!mjr

  Defined functions:
	_bthead(node_num,node,bt)
	_btlink(alpha1,node1,alpha2,node2)
	_btnext(node_num,node,bt)
	_btprevious(node_num,node,bt)
	_bttail(node_num,node,bt)
	_flush_cache_node(bt,cache_node)
	_linknbr(alpha,node1,node_num)
	_nbrlink(node_num,alpha,node1)
	_nodebal(alpha,node1,node2,node3)
	_popl(bt)
	_popr(bt)
	_pushl(bt,node_num)
	_pushr(bt,node_num)
	_rnode(node_num,npt,bt)
	_wnode(node_num,npt,bt)
	_wsuper(bt)
	btclose(bt)
	btdelete(node_num,bt)
	btfind(key,node_num,reckey,recno,bt)
	btflush(bt)
	bthead(node_num,key,recno,bt)
	btinsert(argkey,recno,bt)
	btnext(node_num,key,recno,bt)
	btopen(path,flags,mode,keysize)
	btperror(str)
	btprevious(node_num,key,recno,bt)
	btsetrec(node_num,newrec,bt)
	bttail(node_num,key,recno,bt)

--------------------------------------------------------------------------*/
/*+:EDITS:*/
/*:12-09-1989-17:29-wht-get working on SCO UNIX -- just #defines */
/*:12-17-1988-17:51-wht-write sblk only at close-writes were inefficient */
/*:12-17-1988-17:51-wht-cache now read-write... writes were inefficient */
/*:12-17-1988-12:09-wht-allow open of existing index to specify zero keysize */
/*:12-16-1988-20:38-wht-variable max key length */
/*:12-16-1988-17:15-wht-static procs no longer static, but with '_' prefix */

#define _GNU_SOURCE
#include <glib.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include "btree.h"

/* a memory resident btree super block */
/* including room to hold a disk super block */
struct btree
{
	int		fd;				/* file descriptor */
	struct	btsuper	*sblk, sblk_spot;	/* copy of superblock */
	struct	btstack	rstak;	/* history stack */
	struct	btstack	lstak;	/* history stack */
	struct	btnode_m **cache;	/* read-only cache */
	short	rdonly;			/* true if file opened read-only */
	short	slev;			/* history stack level */
        int csize; /* cache size */

        guchar *baseptr;
        size_t len;
};

static int _btnext(BTNODENUM *node_num, BTNODE *node, BTREE bt);
static int _btprevious(BTNODENUM *node_num, BTNODE *node, BTREE bt);
static int _bthead(BTNODENUM *node_num, BTNODE *node, BTREE bt);
static int _bttail(BTNODENUM *node_num, BTNODE *node, BTREE bt);

static int btraise(int errnum)
{
  bterrno = errnum;
  return BT_ERR;
}

#define ff fprintf
#define se stderr

#if defined(pyr) || defined(sun) || defined(BSD4)
#include <sys/file.h>
#endif

static long cache_hits = 0;
static long cache_no_hits = 0;

#define	BT_NSIZ		(sizeof(struct btnode) + bt->sblk->keysize)
#define	BT_SSIZ		(sizeof(struct btsuper))

/* the errno for btree problems. we use negative # - */
/* so btperror can use the real UNIX errno */
int bterrno = 0;
const char *bterrs[] = 
{
	"No btree error",
	"Bad index magic number (is this an index file?)",
	"History stack overflow",
	"Cannot delete node zero",
	"Open keysize does not match index keysize",
	"Memory allocation failure",
	"Bad superblock (is this an index file?)",
	"error writing superblock",
	"error writing node",
	"error reading node",
	"error flushing node",
	0
};

static size_t pagesize = 0;
#define MAP_PAGE_MASK(ps) (~((ps)-1))
#define MAP_LEN(x) ((((x)+(pagesize)-1)&MAP_PAGE_MASK(pagesize)))

static void
btassure(BTREE bt, size_t length)
{
  size_t map_len, new_map_len;

  if(bt->len >= length)
    return;

  map_len = MAP_LEN(bt->len);
  new_map_len = MAP_LEN(length);

  if(new_map_len != map_len)
    {
      if(!bt->rdonly)
	ftruncate(bt->fd, new_map_len);

      bt->baseptr = mremap(bt->baseptr, map_len, new_map_len, MREMAP_MAYMOVE);
      bt->sblk = (struct btsuper *)bt->baseptr;
    }

  bt->len = length;
}

/* write the btree superblock to disk */
static int
_wsuper(BTREE bt)
{
	if(bt->rdonly)
		return(0);

	if(bt->baseptr)
	  return 0;

#ifdef DEBUG
	ff(se,"-------> write superblock fd=%d\n",bt->fd);
#endif

	if(lseek(bt->fd,0L,SEEK_SET) < 0)
	  {
	    btraise(BT_BAD_WSUPER);
	    return(BT_ERR);
	  }

	if(write(bt->fd,(char *)bt->sblk,BT_SSIZ) != BT_SSIZ)
	  {
	    btraise(BT_BAD_WSUPER);
	    return(BT_ERR);
	  }

	return(0);
}



/* dynamically allocate a control structure for an open btree */
BTREE
btopen(const char *path, int flags, mode_t mode, int keysize)
{
  struct btree *bt;
  register int r;
  struct stat sbuf;

  if(!pagesize)
    pagesize = getpagesize();

  bt = g_new0(struct btree, 1);

  bt->rdonly = ((flags & O_ACCMODE) == O_RDONLY);

  if(bt->rdonly)
    bt->csize = 16;
  bt->csize = 10240;

  bt->fd = open(path, flags, mode);

  if (bt->fd < 0)
    goto errout;

  bt->sblk = &bt->sblk_spot;
#ifdef DEBUG
  ff(se,"----------> open '%s' fd=%d\n",path,bt->fd);
#endif
  r = read(bt->fd, (char *) &bt->sblk_spot, BT_SSIZ);

  /* if read nothing, must be a new guy, right ? */
  if(r == 0 && !bt->rdonly)
    {
      bt->sblk->magic = BT_MAGIC;
      bt->sblk->free = 1;
      bt->sblk->root = 0;
      bt->sblk->list = 0;

      if(keysize > BT_KSIZ)
	{
	  fprintf(stderr,"key size specified for %s (%d) exceeds max of %d\n",
		  path, keysize, BT_KSIZ);
	  goto errout;
	}
		
      if(keysize)
	{
	  keysize++;		/* allow for null at end of record */
	  if(keysize & 1)
	    keysize++;	/* keep even */
	}
      else
	keysize = BT_KSIZ;

      bt->sblk->keysize = keysize;

      if(_wsuper(bt) == 0)
	r = BT_SSIZ;
    }
  else if(r == BT_SSIZ)
    {
      bterrno = 0;

      if(keysize)
	{
	  keysize++;		/* allow for null at end of record */
	  if(keysize & 1)
	    keysize++;	/* keep even */
	}

      if(bt->sblk->magic != BT_MAGIC)
	btraise(BT_BAD_MAGIC);
      else if(keysize && (keysize != bt->sblk->keysize))
	btraise(BT_BAD_KSIZ);
      if(bterrno)
	goto errout;
    }

  /* cleverly check ret value from either read or write */
  if(r != BT_SSIZ)
    goto errout;

  if(fstat(bt->fd, &sbuf))
    goto errout;
  bt->len = MAX(sbuf.st_size, 1);
  if(!bt->rdonly)
    ftruncate(bt->fd, bt->len);
  bt->baseptr = mmap(NULL, MAP_LEN(bt->len), PROT_READ|(bt->rdonly?0:PROT_WRITE), MAP_SHARED, bt->fd, 0);
  if(bt->baseptr == MAP_FAILED)
    bt->baseptr = NULL;

  /* zero the cache -- will fail at 65535 records :-) */
  if(!bt->baseptr)
    {
      bt->cache = g_new(BTNODE *, bt->csize);
      for(r = 0; r < bt->csize; r++)
	{
	  bt->cache[r] = g_new0(BTNODE, 1);
	  bt->cache[r]->node_num = -1;
	  bt->cache[r]->dirty = 0;
	}
    }

  return bt;

 errout:
  if(bt->fd >= 0)
    close(bt->fd);
  g_free(bt);
  return NULL;
}

/* close and deallocate the control structure */
int
btclose(BTREE bt)
{
  register int err = 0;
  register int icache;

  if(!bt->rdonly)
    err = btflush(bt);
  close(bt->fd);

  if(bt->baseptr)
    {
      munmap(bt->baseptr, bt->len);
    }

  if(bt->cache)
    {
      for(icache = 0; icache < bt->csize; icache++)
	g_free(bt->cache[icache]);
      g_free(bt->cache);
    }

  g_free(bt);

  return err;
}

static int
_flush_cache_node(BTREE bt, BTNODE *cache_node)
{
  if((cache_node->node_num == -1) || (!cache_node->dirty))
    return(0);

  if(lseek(bt->fd, ((cache_node->node_num * BT_NSIZ) + BT_SSIZ), SEEK_SET) < 0)
    return btraise(BT_BAD_FLUSH);
  if(write(bt->fd, (char *)&cache_node->n, BT_NSIZ) != BT_NSIZ)
    return btraise(BT_BAD_FLUSH);
  cache_node->dirty = 0;

  return(0);
}

int
btflush(BTREE bt)
{
	register int icache;
	register struct btnode_m *cache_node;

	if(bt->cache)
	  for(icache = 0; icache < bt->csize; icache++)
	    {
	      cache_node = bt->cache[icache];
	      if(_flush_cache_node(bt, cache_node))
		return(BT_ERR);
	    }

	if(_wsuper(bt))
		return(BT_ERR);

	if(bt->baseptr)
	  msync(bt->baseptr, bt->len, MS_SYNC|MS_INVALIDATE);

	return(0);
}

/* write a node to cache */
static int
_wnode(BTNODENUM node_num, BTNODE *npt, BTREE bt)
{
	unsigned int hash = (unsigned int)node_num & (bt->csize - 1);
	register struct btnode_m *cache_node;

#ifdef DEBUG
	ff(se,"_wnode(node_num=%d,fd=%d)\n",node_num,bt->fd);
	ff(se,"       cache node_num=%d\n",cache_node->node_num);
#endif

	if(!node_num)
	  return(0);

	if(bt->baseptr)
	  {
	    char *off_ptr;
	    btassure(bt, BT_SSIZ+((node_num+1)*BT_NSIZ));
	    off_ptr = bt->baseptr + BT_SSIZ + (node_num * BT_NSIZ);
	    memcpy(off_ptr, npt, sizeof(struct btnode) + bt->sblk->keysize);
	    return 0;
	  }

	errno = 0;
	if(bt->rdonly)
	{
		errno = EPERM;	/* would be reported later */
		return(BT_ERR);
	}
	cache_node = bt->cache[hash];
	if(cache_node->node_num != node_num)
	{
		if(_flush_cache_node(bt, cache_node))
		  return btraise(BT_BAD_WNODE);
	}

	/* update cache -  if the write succeeded */
	cache_node->n = npt->n;
	memcpy(cache_node->key, npt->key, bt->sblk->keysize);
	cache_node->node_num = node_num;
	cache_node->dirty = 1;

	return 0;
}

/* read a node from disk */
static int
_rnode(BTNODENUM node_num, BTNODE *npt, BTREE bt)
{
	register int rdcount;
	register long readpos;
	unsigned int hash = (unsigned int)node_num & (bt->csize - 1);
	register struct btnode_m *cache_node = NULL;

#ifdef DEBUG
	ff(se,"_rnode(node_num=%d,fd=%d)\n",node_num,bt->fd);
	ff(se,"       cache node_num=%d\n",cache_node->node_num);
#endif

	if(!node_num)
	{
	  memset(npt, 0, sizeof(struct btnode_m));
	  return(0);
	}

	if(bt->baseptr)
	  {
	    long off;
	    off = (node_num * BT_NSIZ) + BT_SSIZ;
	    if((off + BT_NSIZ) > bt->len)
	      return btraise(BT_BAD_RNODE);

	    memcpy(npt, bt->baseptr + off, BT_NSIZ);
	    npt->node_num = node_num;
	    npt->dirty = 0;

	    bterrno = 0;
	    return 0;
	  }

	if(bt->cache)
	  cache_node = bt->cache[hash];
	if(!cache_node || cache_node->node_num != node_num)
	{
		/* if no cache hit, load from disk */
		cache_no_hits++;
		if(!bt->rdonly)
		{
			if(_flush_cache_node(bt,cache_node))
				return btraise(BT_BAD_RNODE);
		}
		readpos = ((long)node_num * BT_NSIZ) + BT_SSIZ;
		if(lseek(bt->fd, readpos,SEEK_SET) < 0)
				return btraise(BT_BAD_RNODE);
		rdcount = read(bt->fd, &cache_node->n, BT_NSIZ);
		if(rdcount != BT_NSIZ)
		{
#ifdef DEBUG
			if(rdcount >= 0)
				ff(se,"rdcount for node %u was % (should be %d) @ %ld\n",
					node_num,rdcount,BT_NSIZ,readpos);
#endif
			return btraise(BT_BAD_RNODE);
		}
		if(cache_node)
		  {
		    cache_node->node_num = node_num;
		    cache_node->dirty = 0;
		  }
	}
	else
		cache_hits++;

	npt->n = cache_node->n;
	npt->node_num = cache_node->node_num;
	npt->dirty = 0;
	memcpy(npt->key, cache_node->key, bt->sblk->keysize);

	bterrno = 0;
	return(0);
}

static int
_pushl(BTREE bt, BTNODENUM node_num)
{
	if(++ (bt->lstak.sptr) >= STACK_LENGTH)
	{
	  return btraise(BT_BAD_STACK);
	}
	bt->lstak.ele[bt->lstak.sptr] = node_num;
	bt->lstak.lev[bt->lstak.sptr] = ++bt->slev;

	return 0;
}

static int
_pushr(BTREE bt, BTNODENUM node_num)
{
	if(++ (bt->rstak.sptr) >= STACK_LENGTH)
	  return btraise(BT_BAD_STACK);
	bt->rstak.ele[bt->rstak.sptr] = node_num;
	bt->rstak.lev[bt->rstak.sptr] = ++bt->slev;
	return 0;
}



static BTNODENUM
_popr(BTREE bt)
{

	bt->slev = bt->rstak.lev[bt->rstak.sptr];

	while(bt->lstak.lev[bt->lstak.sptr] > bt->slev)
		(bt->lstak.sptr)--;

	if(bt->rstak.sptr == 0)
		return(0);

	bt->slev--;
	return(bt->rstak.ele[(bt->rstak.sptr)--]);
}



static BTNODENUM
_popl(BTREE bt)
{
	bt->slev = bt->lstak.lev[bt->lstak.sptr];

	while(bt->rstak.lev[bt->rstak.sptr] > bt->slev)
		(bt->rstak.sptr)--;

	if(bt->lstak.sptr == 0)
		return(0);

	bt->slev--;
	return(bt->lstak.ele[(bt->lstak.sptr)--]);
}

static void
_btlink(int alpha1, BTNODE *node1, int alpha2, BTNODE *node2)
{
	if(alpha1 == -1 && alpha2 == -1)
		node1->n.lptr = node2->n.lptr;
	else if(alpha1 == -1 && alpha2 == 1)
		node1->n.lptr = node2->n.rptr;
	else if(alpha1 == 1 && alpha2 == -1)
		node1->n.rptr = node2->n.lptr;
	else
		node1->n.rptr = node2->n.rptr;
}



/* number a link according to alpha */
static void
_nbrlink(BTNODENUM *node_num, int alpha, BTNODE *node1)
{
	*node_num = (alpha == 1) ? node1->n.rptr : node1->n.lptr;
}

/* set a link according to alpha */
void
_linknbr(int alpha, BTNODE *node1, BTNODENUM node_num)
{
	if(alpha == 1)
		node1->n.rptr = node_num;
	else
		node1->n.lptr = node_num;
}



static void
_nodebal(int alpha, BTNODE *node1, BTNODE *node2,
	 BTNODE *node3)
{
	if(node1->n.balance == alpha)
	{
		node2->n.balance = -alpha;
		node3->n.balance = 0;
	}
	else if(node1->n.balance == 0)
		node2->n.balance = node3->n.balance = 0;
	else 
	{
		node2->n.balance = 0;
		node3->n.balance = alpha;
	}
}



/* change the record number in a node */
int
btsetrec(BTREE bt, BTNODENUM node_num, BTRECNUM newrec)
{
	struct btnode_m tnode;

	if(_rnode(node_num, &tnode, bt))
		return(BT_ERR);

	tnode.n.recno = newrec;

	if(_wnode(node_num, &tnode, bt))
		return(BT_ERR);

	return(0);
}

/* insert the node into the tree */
int
btinsert(BTREE bt, const char *argkey, BTRECNUM recno)
{
	register int compare;
	BTNODENUM trec1;
	BTNODENUM trec2;
	BTNODENUM trec3;
	BTNODENUM trec4;
	BTNODENUM top;
	struct btnode_m tnode1;
	struct btnode_m tnode2;
	struct btnode_m tnode3;
	struct btnode_m tnode4;

	/* the very first node gets inserted specially */
	if(bt->sblk->root == 0)
	{
		bt->sblk->root = 1;

		tnode1.n.balance = tnode1.n.rptr = tnode1.n.lptr = 0;
		tnode1.n.deleted = BT_ACTIVE;
		tnode1.n.recno = recno;

		strncpy(tnode1.key,argkey,bt->sblk->keysize - 1);
		tnode1.key[bt->sblk->keysize - 1] = '\0';
		if(_wnode(1,&tnode1,bt) < 0)
			return(BT_ERR);

		bt->sblk->free++;
		bt->sblk->list++;
		return 1;
	}
	top = -1;
	trec1 = bt->sblk->root;
	trec4 = bt->sblk->root;
	while(1)
	{
		if(_rnode(trec1,&tnode1,bt))
		{
#ifdef DEBUG
			ff(se,"btinsert: trec1(1) read error\n");
#endif
			return(BT_ERR);
		}
		if((compare = strcmp(argkey,tnode1.key)) < 0)
		{
			/* (move left) */
			trec2 = tnode1.n.lptr;

			if(trec2 == 0)
			{
				/* insert here */
				trec2 = bt->sblk->free++;
				tnode1.n.lptr = trec2;
				break;	/* loop exit */
			}
			else 
			{
				/* look again from this node */
				if(_rnode(trec2,&tnode2,bt))
				{
#ifdef DEBUG
					ff(se,"btinsert: trec2(1) read error\n");
#endif
					return(BT_ERR);
				}
				if(tnode2.n.balance != 0)
				{
					top = trec1;
					trec4 = trec2;
				}
			}
			trec1 = trec2;
		}
		else 
		{
			/* (move right) */
			trec2 = tnode1.n.rptr;

			if(trec2 == 0)
			{
				/* insert here */
				trec2 = bt->sblk->free++;
				tnode1.n.rptr = trec2;
				break;	/* loop exit */
			}
			else 
			{
				/* look again from this node */
				if(_rnode(trec2,&tnode2,bt))
				{
#ifdef DEBUG
					ff(se,"btinsert: trec2(2) read error\n");
#endif
					return(BT_ERR);
				}
				if(tnode2.n.balance != 0)
				{
					top = trec1;
					trec4 = trec2;
				}
				trec1 = trec2;
			}
		}
	}

	/* Step 5 (insert key at tnode2) */
	tnode2.n.lptr = tnode2.n.rptr = 0;
	tnode2.n.balance = 0;
	tnode2.n.deleted = BT_ACTIVE;
	tnode2.n.recno = recno;
	strncpy(tnode2.key,argkey,bt->sblk->keysize - 1);
	tnode2.key[bt->sblk->keysize - 1] = '\0';

	if(_wnode(trec2,&tnode2,bt))
		return(BT_ERR);
	if(_wnode(trec1,&tnode1,bt))
		return(BT_ERR);
	if(_rnode(trec4,&tnode4,bt))
	{
#ifdef DEBUG
		ff(se,"btinsert: trec4(1) read error\n");
#endif
		return(BT_ERR);
	}

	/* (adjust balance factors) */
	if(strcmp(argkey,tnode4.key) < 0)
		trec3 = trec1 = tnode4.n.lptr;
	else 
		trec3 = trec1 = tnode4.n.rptr;

	while(trec1 != trec2)
	{
		if(_rnode(trec1,&tnode1,bt))
		{
#ifdef DEBUG
			ff(se,"btinsert: trec1(2) read error\n");
#endif
			return(BT_ERR);
		}
		if(strcmp(argkey,tnode1.key) < 0)
		{
			tnode1.n.balance = -1;
			if(_wnode(trec1,&tnode1,bt) < 0)
				return(BT_ERR);
			trec1 = tnode1.n.lptr;
		}
		else 
		{
			tnode1.n.balance = 1;
			if(_wnode(trec1,&tnode1,bt) < 0)
				return(BT_ERR);
			trec1 = tnode1.n.rptr;
		}
	}

	compare = (strcmp(argkey,tnode4.key) < 0) ? -1 : 1;
	if(tnode4.n.balance == 0)
	{
		bt->sblk->list++;
		tnode4.n.balance = compare;
		if(_wnode(trec4,&tnode4,bt) < 0)
			return(BT_ERR);
		return trec2;
	}
	else if(tnode4.n.balance == -compare)
	{
		bt->sblk->list++;
		tnode4.n.balance = 0;
		if(_wnode(trec4,&tnode4,bt) < 0)
			return(BT_ERR);
		return trec2;
	}
	else 
	{
		/* (tree out of balance) */
		bt->sblk->list++;
		if(_rnode(trec4,&tnode4,bt))
		{
#ifdef DEBUG
			ff(se,"btinsert: trec4(2) read error\n");
#endif
			return(BT_ERR);
		}
		if(_rnode(trec3,&tnode3,bt))
		{
#ifdef DEBUG
			ff(se,"btinsert: trec3(1) read error\n");
#endif
			return(BT_ERR);
		}
		if(tnode3.n.balance == compare)
		{
			/* (single rotate) */
			trec1 = trec3;
			_btlink(compare, &tnode4,
				-compare, &tnode3);
			_linknbr(-compare, &tnode3,trec4);
			tnode4.n.balance = tnode3.n.balance = 0;
		}
		else 
		{
			/* (double rotate) */
			_nbrlink(&trec1,-compare, &tnode3);
			if(_rnode(trec1, &tnode1,bt))
			{
#ifdef DEBUG
				ff(se,"btinsert: trec1(3) read error\n");
#endif
				return(BT_ERR);
			}
			_btlink(-compare, &tnode3,compare, &tnode1);
			_linknbr(compare,&tnode1,trec3);
			_btlink(compare,&tnode4,-compare,&tnode1);
			_linknbr(-compare,&tnode1,trec4);
			_nodebal(compare,&tnode1,&tnode4,&tnode3);
			tnode1.n.balance = 0;
			if(_wnode(trec1,&tnode1,bt))
				return(BT_ERR);
		}

		if(top == -1)
			bt->sblk->root = trec1;
		else 
		{
			/* balanced at top of a sub-tree */
			if(_rnode(top,&tnode2,bt) < 0)
			{
#ifdef DEBUG
				ff(se,"btinsert: top(1) read error\n");
#endif
				return(BT_ERR);
			}
			if(trec4 == tnode2.n.rptr)
				tnode2.n.rptr = trec1;
			else
				tnode2.n.lptr = trec1;
			if(_wnode(top,&tnode2,bt))
				return(BT_ERR);
		}
		if(_wnode(trec4,&tnode4,bt))
			return(BT_ERR);
		if(_wnode(trec3,&tnode3,bt))
			return(BT_ERR);
		return trec2;
	}
}

/* drop a node */
int btdelete(BTREE bt, BTNODENUM node_num)
{
	struct btnode_m node;

	if(node_num == 0)
	  return btraise(BT_BAD_ROOT);
	else 
	{
		if(_rnode(node_num,&node,bt))
			return(BT_ERR);
		node.n.deleted = BT_DELETED;
		if(_wnode(node_num,&node,bt))
			return(BT_ERR);
		else 
			bt->sblk->list--;
	}
	return(0);
}

/* find the next node */
int btnext(BTREE bt, BTNODENUM *node_num, char ** const key, BTRECNUM *recno)
{
	static struct btnode_m node;
	register int retn;
	if(_rnode(*node_num,&node,bt))
		return(BT_ERR);
	retn = _btnext(node_num,&node,bt);
	*key = node.key;
	*recno = node.n.recno;
	return(retn);
}


/* find the next node */
static int
_btnext(BTNODENUM *node_num, BTNODE *node, BTREE bt)
{
	BTNODENUM s_nod;

	s_nod = *node_num;

	if(*node_num == 0)
	{
		/* undefined current node - wind to beginning of file */
		return(_bthead(node_num, node, bt));
	}
	do 
	{
		if(node->n.rptr == 0)
		{
			/* can't move right */
			if(bt->lstak.sptr == 0)
			{
				/* none in stack */
				if(_rnode(*node_num, node, bt))
					return(BT_ERR);
				return(BT_EOF);
			}
			else 
			{
				/* can't go right & stack full (pop stack) */
				s_nod = _popl(bt);
				if(_rnode(s_nod, node, bt) < 0)
					return(BT_ERR);
			}
		}
		else 
		{
			/* move right */
			_pushr(bt, s_nod);
			s_nod = node->n.rptr;
			if(_rnode(s_nod, node,bt) )
				return(BT_ERR);

			while(node->n.lptr != 0)
			{
				/* bottom left */
				_pushl(bt, s_nod);
				/* of this sub-tree */
				s_nod = node->n.lptr;
				if(_rnode(s_nod, node,bt))
					return(BT_ERR);
			}
		}
	} while(node->n.deleted == BT_DELETED);

	*node_num = s_nod;
	return(0);
}

/* go to the tail of the file */
int
bttail(BTREE bt, BTNODENUM *node_num, char ** const key, BTRECNUM *recno)
{
	static struct btnode_m node;
	register int retn = _bttail(node_num,&node,bt);
	*key = node.key;
	*recno = node.n.recno;
	return(retn);
}

/* go to the tail of the file */
static int
_bttail(BTNODENUM *node_num, BTNODE *node, BTREE bt)
{
	BTNODENUM s_nod;

	bt->rstak.sptr = 0;
	bt->lstak.sptr = 0;
	bt->rstak.ele[0] = 0;
	bt->lstak.ele[0] = 0;

	/* begin at list head */
	s_nod = bt->sblk->root;

	if(_rnode(s_nod,node,bt))
		return(BT_ERR);
	while(1)
	{
		/* search to right */
		if(node->n.rptr != 0)
		{
			_pushr(bt,s_nod);
			s_nod = node->n.rptr;
			if(_rnode(s_nod,node,bt))
				return(BT_ERR);
		}
		else 
		{
			if(node->n.deleted == BT_DELETED)
			{
				/* skip all deleted nodes */
				while(node->n.deleted == BT_DELETED)
				{
					if(_btnext(&s_nod,node,bt) == BT_EOF)
					{
						if(_btprevious(&s_nod,node,bt) <0)
							return(BT_ERR);
						*node_num = s_nod;
						return(0);
					}
				}
				*node_num = s_nod;
				return(0);
			}
			else 
			{
				/* at end of a branch */
				*node_num = s_nod;
				return(0);
			}
		}
	}
}


/* go to the head of the file */
int
bthead(BTREE bt, BTNODENUM *node_num, char ** const key, BTRECNUM *recno)
{
	static struct btnode_m node;
	register int retn = _bthead(node_num,&node,bt);
	*key = node.key;
	*recno = node.n.recno;
	return(retn);
}

/* go to the head of the file */
static int
_bthead(BTNODENUM *node_num, BTNODE *node, BTREE bt)
{
	BTNODENUM s_nod;

	bt->rstak.sptr = 0;
	bt->lstak.sptr = 0;
	bt->rstak.ele[0] = 0;
	bt->lstak.ele[0] = 0;

	/* begin at list head */
	s_nod = bt->sblk->root;

	if(_rnode(s_nod,node,bt))
		return(BT_ERR);
	while(1)
	{
		/* search to left */
		if(node->n.lptr != 0)
		{
			_pushl(bt,s_nod);
			s_nod = node->n.lptr;
			if(_rnode(s_nod,node,bt))
				return(BT_ERR);
		}
		else 
		{
			if(node->n.deleted == BT_DELETED)
			{
				/* skip all deleted nodes */
				while(node->n.deleted == BT_DELETED)
				{
					if(_btprevious(&s_nod,node,bt) == BT_EOF)
					{
						if(_btnext(&s_nod,node,bt) <0)
							return(BT_ERR);
						*node_num = s_nod;
						return(0);
					}
				}
				*node_num = s_nod;
				return(0);
			}
			else 
			{
				/* at end of a branch */
				*node_num = s_nod;
				return(0);
			}
		}
	}
}



/* find a key */
int
btfind(BTREE bt, const char *key, BTNODENUM *node_num, char ** const reckey,
       BTRECNUM *recno)
{
	register int direction;
	BTNODENUM s_nod;
	static struct btnode_m node;

	bt->rstak.sptr = 0;
	bt->lstak.sptr = 0;
	bt->rstak.ele[0] = 0;
	bt->lstak.ele[0] = 0;

	bt->slev = 0;		/* tree level at start of search */

	/* begin at list head */
	s_nod = bt->sblk->root;

	if(_rnode(s_nod,&node,bt))
		return(BT_ERR);
	while((direction = strcmp(key,node.key)) != 0 ||
	    node.n.deleted == BT_DELETED)
	{

		if(direction > 0)
		{
			/* search to right */
			if(node.n.rptr != 0)
			{
				_pushr(bt,s_nod);
				s_nod = node.n.rptr;
				if(_rnode(s_nod,&node,bt))
					return(BT_ERR);
			}
			else if(node.n.deleted == BT_DELETED)
			{
				/* skip all deleted nodes */
				while(node.n.deleted == BT_DELETED)
				{
					if(_btnext(&s_nod,&node,bt) == BT_EOF)
					{
						if(_btprevious(&s_nod,&node,bt) <0)
							return(BT_ERR);
						*recno = node.n.recno;
						*reckey = node.key;
						*node_num = s_nod;
						return(BT_NREC);
					}
				}
				*recno = node.n.recno;
				*reckey = node.key;
				*node_num = s_nod;
				return(BT_NREC);
			}
			else 
			{
				/* at end of a branch */
				*recno = node.n.recno;
				*reckey = node.key;
				*node_num = s_nod;
				return(BT_NREC);
			}
		}
		else 
		{
			/* search to left */
			if(node.n.lptr != 0)
			{
				_pushl(bt,s_nod);
				s_nod = node.n.lptr;
				if(_rnode(s_nod,&node,bt))
					return(BT_ERR);
			}
			else if(node.n.deleted == BT_DELETED)
			{
				while(node.n.deleted == BT_DELETED)
				{
					if(_btnext(&s_nod,&node,bt) == BT_EOF)
					{
						if(_btprevious(&s_nod,&node,bt) < 0)
							return(BT_ERR);
						*recno = node.n.recno;
						*reckey = node.key;
						*node_num = s_nod;
						return(BT_NREC);
					}
				}
				*recno = node.n.recno;
				*reckey = node.key;
				*node_num = s_nod;
				return(BT_NREC);
			}
			else 
			{
				*recno = node.n.recno;
				*reckey = node.key;
				*node_num = s_nod;
				return(BT_NREC);
			}
		}
	}
	*recno = node.n.recno;
	*reckey = node.key;
	*node_num = s_nod;
	return(0);
}

/* find the previous node */
int
btprevious(BTREE bt, BTNODENUM *node_num, char ** const key, BTRECNUM *recno)
{
	static struct btnode_m node;
	register int retn;
	if(_rnode(*node_num,&node,bt))
		return(BT_ERR);
	retn = _btprevious(node_num,&node,bt);
	*key = node.key;
	*recno = node.n.recno;
	return(retn);
}

/* get the previous node */
static int
_btprevious(BTNODENUM *node_num, BTNODE *node, BTREE bt)
{
	BTNODENUM s_nod;

	s_nod = *node_num;

	/* if we are called without a node, wind to the end of file */
	if(s_nod == 0)
		return(_bttail(node_num,node,bt));

	do 
	{
		if(node->n.lptr == 0)
		{
			/* can't move left */
			if(bt->rstak.sptr == 0)
			{
				/* none in stack */
				if(_rnode(*node_num,node,bt))
					return(BT_ERR);
				return(BT_EOF);
				/* don't reset node_num */
			}
			else 
			{
				/* can't go left & stack full (pop stack) */
				s_nod = _popr(bt);
				if(_rnode(s_nod,node,bt))
					return(BT_ERR);
			}
		}
		else 
		{
			/* left then to bottom right - is previous */
			_pushl(bt,s_nod);
			s_nod = node->n.lptr;
			if(_rnode(s_nod,node,bt))
				return(BT_ERR);
			while(node->n.rptr != 0)
			{
				/* bottom right */
				_pushr(bt,s_nod);
				/* of this sub-tree */
				s_nod = node->n.rptr;
				if(_rnode(s_nod,node,bt))
					return(BT_ERR);
			}
		}
	} while(node->n.deleted == BT_DELETED);

	*node_num = s_nod;
	return(0);
}

/* print the btree error message */
void
btperror(const char *str)
{
	/* is it ours ?? */
	if(bterrno)
	{
		ff(se,"(btree) %s: %s\n",str,bterrs[bterrno]);
		bterrno = 0;
	}
	if(errno)
	{
		fputs("(filesys) ", se);
		perror(str);
		errno = 0;
	}
}

/* vi: set tabstop=4 shiftwidth=4: */
/* end of btree.c */
