/*
 * Copyright (C) 1988, Marcus J. Ranum, William Welch Medical Library
 * $Author$
 */

/*
 * $Log$
 * Revision 1.1  1999/11/12 15:47:05  sopwith
 * Initial revision
 *
 * Revision 1.1  88/06/01  21:35:24  mjr
 * Initial revision
 * 
 */


/* used to load a tree with data from the standard input - */
/* primarily for testing purposes - to load a file with some */
/* "known" data, which can then be dumped and checked, etc */

#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include "btree.h"

int
main(int ac, char *av[])
{
	BTREE	bt;
	long	time();
	long	start;
	long	end;
	long	lded = 0L;
	char	buf[BUFSIZ];

	if(ac < 2) {
		(void)fprintf(stderr,"usage: %s <file>\n",av[0]);
		exit(1);
	}

	(void)srand(getpid());

	if((bt = btopen(av[1],O_RDWR|O_CREAT,0600,50)) ==NULL) {
		btperror(av[1]);
		exit(1);
	}

	(void)time(&start);

	while(gets(buf)) {
		/* note - btinsert() truncates overlong keys */
		if(btinsert(buf, rand(), bt)< 0) {
			btperror("insert");
			(void)btclose(bt);
			exit(1);
		} else 
			lded++;
	}
	(void)btclose(bt);
	(void)time(&end);
	(void)fprintf(stderr,"total secs:%ld - %ld records\n",end-start,lded);
	exit(0);
}
