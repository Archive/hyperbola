/*
 * Copyright (C) 1988, Marcus J. Ranum, William Welch Medical Library
 * $Author$
 */

/*
 * $Log$
 * Revision 1.1  1999/11/12 15:47:05  sopwith
 * Initial revision
 *
 * Revision 1.1  88/06/01  21:35:34  mjr
 * Initial revision
 * 
 */


/* provides some minimally useful info about how big your */
/* data structures are */
#include <stdio.h>
#include "btree.h"

int
main(int argc, char *argv[])
{
	printf("a btree control structure is %d bytes",sizeof(BTREE));
	printf(" (cache of %d nodes)\n",BT_CSIZ);
	printf("a btree node is %d bytes\n",sizeof(BTNODE));
	return 0;
}
