/*+-------------------------------------------------------------------------
	btree.h
	hacked by ...!gatech!kd4nc!n4hgf!wht

Copyright (C) 1988, Marcus J.  Ranum, William Welch Medical Library
$Author$ $Log$
$Author: sopwith $ Revision 1.2  1999/11/18 21:18:35  sopwith
$Author: sopwith $ Woohoo, add basic GUI object framework.
$Author: sopwith $
$Author$ Revision 1.1.1.1  1999/11/12 15:47:05  sopwith
$Author$
$Author$
$Author$ Initial import.
$Author$ Revision 1.1 88/06/01 21:35:07 mjr
Initial revision
The original code was placed in the public domain.  This is not, since I
wish to retain some control over it.  No restrictions are placed on
modification, or use of this code, as long as the copyrights are not
removed.  There are some areas that really could use improving (like
handling free nodes better) and I hope that if someone makes
improvements they will keep me up to date on them (e-mail please, I am
not on usenet).
Marcus J. Ranum, William Welch Medical Library, 1988
mjr@jhuigf.BITNET || uunet!mimsy!aplcen!osiris!welchvax!mjr
--------------------------------------------------------------------------*/
/*+:EDITS:*/
/*:12-10-1989-18:03-wht-make cache much larger */
/*:12-17-1988-19:52-wht-released 1.20  */
/*:12-16-1988-20:38-wht-variable max key length */
/*:12-16-1988-18:22-afterlint-creation */

#ifndef _INCL_BTREE_H
#define _INCL_BTREE_H 1

#include <glib.h>
#include <sys/types.h>

#define BT_NREC	1	/* no such record */
#define BT_EOF	2	/* end of the tree (either end) */
#define BT_ERR	-1	/* something went wrong */

#define BT_KSIZ	4094	/* size of keys to store (or trunc) */

/* this indicates a node deleted */
#define	BT_DELETED	1
#define	BT_ACTIVE	0

#define	BT_MAGIC	0x0BB1

/* btree stack */
#define STACK_LENGTH 30		/* length of history stacks */

typedef gint32 BTNODENUM;
typedef gint32 BTRECNUM;

struct btstack {
	BTNODENUM	ele[STACK_LENGTH];	/* stack elements */
	short	lev[STACK_LENGTH];	/* stack levels */
	short	sptr;				/* stack pointer */
};

/* a disk resident btree super block */
struct	btsuper {
	gint16	magic;		/* magic number */
	gint16	keysize;	/* maximum length of key */
	BTNODENUM	root;		/* pointer to root node */
	BTNODENUM	free;		/* pointer to next free (basically, EOF) */
	gint32	list;		/* number of active records */
};

struct btnode {
	BTRECNUM	recno;		/* index to external file, or whatever */
	BTNODENUM	lptr;		/* left-pointer */
	BTNODENUM	rptr;		/* right-pointer */
	char	deleted;	/* deleted flag */
	char	balance;	/* balance flag */
};

struct btnode_m
{
	struct btnode n;		/* keep these two ... */
	char key[BT_KSIZ];		/* ... fields FIRST !!! */
	BTNODENUM node_num;			/* disk node number */
	char dirty;			/* if true, cache item needs writing to disk */
};

typedef struct btree *BTREE;
typedef struct btnode_m BTNODE;

extern	int	bterrno;	/* btree error number/flag */
extern	const char	*bterrs[];	/* error message list */
/* error codes - match bterrs */
#define	BT_BAD_MAGIC	1	/* bad index file magic number */
#define	BT_BAD_STACK	2	/* history stack overflow */
#define	BT_BAD_ROOT	3	/* failed attempt to delete node 0 */
#define BT_BAD_KSIZ	4	/* keysize at open does not match index keysize */
#define BT_BAD_MEMORY	5	/* memory allocation failed */
#define BT_BAD_SBLK	6	/* bad superblock */
#define BT_BAD_WSUPER	7	/* error writing superblock */
#define BT_BAD_WNODE	8	/* error writing node */
#define BT_BAD_RNODE	9	/* error reading node */
#define BT_BAD_FLUSH	10	/* error flushing node */

BTREE btopen(const char *path, int flags, mode_t mode, int keysize);
int btclose(BTREE bt);
int btflush(BTREE bt);
int btsetrec(BTREE bt, BTNODENUM node_num, BTRECNUM newrec);
BTNODENUM btinsert(BTREE bt, const char *argkey, BTRECNUM recno); /* will return -1 on error */
int btdelete(BTREE bt, BTNODENUM node_num);
int btnext(BTREE bt, BTNODENUM *node_num, char ** const key, BTRECNUM *recno);
int bttail(BTREE bt, BTNODENUM *node_num, char ** const key, BTRECNUM *recno);
int bthead(BTREE bt, BTNODENUM *node_num, char ** const key, BTRECNUM *recno);
int btfind(BTREE bt, const char *key, BTNODENUM *node_num, char ** const reckey,
	   BTRECNUM *recno);
int btprevious(BTREE bt, BTNODENUM *node_num, char ** const key,
	       BTRECNUM *recno);
void btperror(const char *str);

#endif /*_INCL_BTREE_H */
/* vi: set tabstop=4 shiftwidth=4: */
/* end of btree.h */
