/*
 * Copyright (C) 1988, Marcus J. Ranum, William Welch Medical Library
 * $Author$
 */

/*
 * $Log$
 * Revision 1.1  1999/11/12 15:47:05  sopwith
 * Initial revision
 *
 * Revision 1.1  88/06/01  21:35:46  mjr
 * Initial revision
 * 
 */


/* dump a tree to the standard output */

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "btree.h"

int
main(int ac, char *av[])
{
	BTREE	bt;
	BTNODENUM nbr =0L;
	char *key;
	BTNODENUM node_num;
	char *reckey;
	BTRECNUM recno;
	int i;

	if(ac < 2) {
		fprintf(stderr,"usage: %s <file>\n",av[0]);
		exit(1);
	}

	if((bt = btopen(av[1],O_RDONLY,0600, 50)) == NULL) {
		btperror(av[1]);
		exit(1);
	}

	for(i = 2; i < ac; i++)
	{
		switch (btfind(av[i], &node_num, &reckey, &recno, bt)) {
		case (-1):
			btperror("btnext");
			btclose(bt);
			exit(1);

		case (0):
			printf("%s\n", reckey);
			break;

		case (BT_EOF):
			btclose(bt);
			exit(0);
		}
	}

	if (ac < 3)
	/* if btnext is called with nbr = 0, it automatically winds to the */
	/* front of the file - as in this example */
	while (1) {
		switch (btnext(&nbr, &key, &recno, bt)) {
		case (-1):
			btperror("btnext");
			btclose(bt);
			exit(1);

		case (0):
			printf("%s\n", key);
			break;

		case (BT_EOF):
			btclose(bt);
			exit(0);
		}
	}
	btclose(bt);

	return 0;
}
