/*
 * Copyright (C) 1988, Marcus J. Ranum, William Welch Medical Library
 * $Author$
 */

/*
 * $Log$
 * Revision 1.1  1999/11/12 15:47:05  sopwith
 * Initial revision
 *
 * Revision 1.1  88/06/01  21:34:55  mjr
 * Initial revision
 * 
 */


/* grosso hack to load a database and perform a bunch of */
/* operations on it - a sort of benchmark, but not in any */
/* real sense of the word */

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include "btree.h"

#define NRECS 50000

int
main(int ac, char *av[])
{
	BTREE	bt;
	char *key;
	BTRECNUM rec;
	int	fo;
	int	xo;
	BTNODENUM nbr;
	char	buf[120];
	struct timeval tv1, tv2;

	if(ac < 2) {
		fprintf(stderr,"usage: %s <file>\n",av[0]);
		return 1;
	}

	srand(getpid());
	if((bt = btopen(av[1],O_CREAT|O_TRUNC|O_RDWR,0600, 20)) == NULL) {
		btperror(av[1]);
		return 1;
	}

	gettimeofday(&tv1, NULL);

	fprintf(stderr,"insert\n");
	/* insert random records */
	for(fo = 0; fo < NRECS; fo++) {
		for(xo = 0; xo < ((int)rand()%15)+1; xo++)
			buf[xo] = (int)((rand()%25)+97);
		buf[xo] = '\0';
		if(btinsert(buf, fo, bt)<0) {
			btperror("insert");
			return 1;
		}
	}

	fprintf(stderr,"find\n");
	/* find random records */
	for(fo = 0; fo < NRECS; fo++) {
		for(xo = 0; xo < ((int)rand()%15)+1; xo++)
			buf[xo] = (int)((rand()%25)+97);
		buf[xo] = '\0';
		if(btfind(buf, &nbr, &key, &rec, bt)<0) {
			btperror("find");
			return 1;
		}
	}

	fprintf(stderr,"delete\n");
	for(fo = 0; fo < (NRECS/10); fo++) {
		for(xo = 0; xo < ((int)rand()%15)+1; xo++)
			buf[xo] = (int)(rand()%25)+97;
		buf[xo] = '\0';
		if(btfind(buf, &nbr, &key, &rec, bt)<0) {
			btperror("find");
			exit(1);
		}
		if(nbr != 0L && btdelete(nbr, bt)<0) {
			btperror("delete");
			fprintf(stderr,"node=%d\n", nbr);
			exit(1);
		}
	}

	btclose(bt);
	gettimeofday(&tv2, NULL);
	tv2.tv_sec -= tv1.tv_sec;
	tv2.tv_usec -= tv1.tv_usec;
	if(tv2.tv_usec < 0)
	{
		tv2.tv_usec += 1000000;
		tv2.tv_sec++;
	}

	fprintf(stderr,"total secs:%d.%d\n",tv2.tv_sec, tv2.tv_usec);
	return 0;
}
