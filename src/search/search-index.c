#include <libgnome/libgnome.h>
#include <limits.h>
#include <btree.h>
#include <recio.h>
#include <dirent.h>
#include <sys/mman.h>
#include <ctype.h>
#include <unistd.h>

typedef struct {
  BTREE bt_w2f, bt_fn;
  RIFD rf_w2f, rf_fn;
  BTRECNUM file_id, word_id;
} IndexContext;

#define NFILE_CHECK 1000

static void index_find(IndexContext *ctx, const char *word);

int main(int argc, char *argv[])
{
  IndexContext ctx;
  char cbuf[PATH_MAX];
  int i;

  if(argc < 3) {
    g_printerr("Usage: %s <index directory> <words to find>\n", argv[0]);
    return 1;
  }

  if(!g_file_test(argv[1], G_FILE_TEST_ISDIR)) /* Try creating the directory */
    mkdir(argv[1], 0666);

  if(!g_file_test(argv[1], G_FILE_TEST_ISDIR))
    {
      g_printerr("%s: %s is not a directory\n", argv[0], argv[1]);
      return 1;
    }

  ctx.file_id = ctx.word_id = 1;
  g_snprintf(cbuf, sizeof(cbuf), "%s/word2file.idx", argv[1]);
  ctx.bt_w2f = btopen(cbuf, O_RDONLY, 0644, 64);
  g_return_val_if_fail(ctx.bt_w2f, 1);

  g_snprintf(cbuf, sizeof(cbuf), "%s/word2file", argv[1]);
  ctx.rf_w2f = riopen(cbuf, O_RDONLY, 0644, 64);

  g_snprintf(cbuf, sizeof(cbuf), "%s/filename.idx", argv[1]);
  ctx.bt_fn = btopen(cbuf, O_RDONLY, 0644, 256);

  g_snprintf(cbuf, sizeof(cbuf), "%s/filename", argv[1]);
  ctx.rf_fn = riopen(cbuf, O_RDONLY, 0644, 64);

  for(i = 2; i < argc; i++)
    index_find(&ctx, argv[i]);

  btclose(ctx.bt_w2f);
  btclose(ctx.bt_fn);
  riclose(ctx.rf_w2f);
  riclose(ctx.rf_fn);

  return 0;
}

static void
index_find(IndexContext *ctx, const char *word)
{
  BTRECNUM file_check[NFILE_CHECK];
  int r;
  RIREC word_id;
  BTNODENUM node_num;
  char *key;
  char file_name[PATH_MAX];

  r = btfind(ctx->bt_w2f, word, &node_num, &key, &word_id);
  if(r)
    {
      g_print("Word %s not found in index\n", word);
      return;
    }

  for(r = 0; ;) {
    int nitems;
    int i, slen;

    r = riread(ctx->rf_w2f, word_id, file_check, sizeof(file_check), (r>0)?TRUE:FALSE);
    if(r <= 0) break;

    nitems = r/sizeof(file_check[0]);
    g_print("%d matches for %s:\n", nitems, word);
    for(i = 0; i < nitems; i++)
      {
	slen = riread(ctx->rf_fn, file_check[i], file_name, sizeof(file_name), FALSE);
	if(slen < 0)
	  continue;
	file_name[slen] = '\0';
	g_print("   %s\n", file_name);
      }
  };
}
