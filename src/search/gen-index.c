#include <libgnome/libgnome.h>
#include <limits.h>
#include <btree.h>
#include <recio.h>
#include <dirent.h>
#include <sys/mman.h>
#include <ctype.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>

typedef struct {
  BTREE bt_w2f, bt_fn;
  RIFD rf_w2f, rf_fn;
  BTRECNUM file_id, word_id;
} IndexContext;

#define NFILE_CHECK 1000

static void index_path(IndexContext *ctx, const char *path);
static void filename_add(IndexContext *ctx, const char *path, BTRECNUM file_id);
static void word_add(IndexContext *ctx, const char *word, BTRECNUM file_id);

int main(int argc, char *argv[])
{
  IndexContext ctx;
  char cbuf[PATH_MAX];
  int i;
  struct timeval tv, tv2;

  if(argc < 3) {
    g_printerr("Usage: %s <index directory> <locations to index>\n", argv[0]);
    return 1;
  }

  if(!g_file_test(argv[1], G_FILE_TEST_ISDIR)) /* Try creating the directory */
    mkdir(argv[1], 0666);

  if(!g_file_test(argv[1], G_FILE_TEST_ISDIR))
    {
      g_printerr("%s: %s is not a directory\n", argv[0], argv[1]);
      return 1;
    }

  ctx.file_id = ctx.word_id = 1;
  g_snprintf(cbuf, sizeof(cbuf), "%s/word2file.idx", argv[1]);
  ctx.bt_w2f = btopen(cbuf, O_RDWR|O_CREAT|O_TRUNC, 0644, 64);
  g_return_val_if_fail(ctx.bt_w2f, 1);

  g_snprintf(cbuf, sizeof(cbuf), "%s/word2file", argv[1]);
  ctx.rf_w2f = riopen(cbuf, O_RDWR|O_CREAT|O_TRUNC, 0644, 64);

  g_snprintf(cbuf, sizeof(cbuf), "%s/filename.idx", argv[1]);
  ctx.bt_fn = btopen(cbuf, O_RDWR|O_CREAT|O_TRUNC, 0644, 256);

  g_snprintf(cbuf, sizeof(cbuf), "%s/filename", argv[1]);
  ctx.rf_fn = riopen(cbuf, O_RDWR|O_CREAT|O_TRUNC, 0644, 64);

  gettimeofday(&tv, NULL);

  for(i = 2; i < argc; i++)
    index_path(&ctx, argv[i]);

  btclose(ctx.bt_w2f);
  btclose(ctx.bt_fn);
  riclose(ctx.rf_w2f);
  riclose(ctx.rf_fn);

  gettimeofday(&tv2, NULL);
  tv2.tv_sec -= tv.tv_sec;
  tv2.tv_usec -= tv.tv_usec;
  if(tv2.tv_usec < 0)
    {
      tv2.tv_usec += 1000000;
      tv2.tv_sec--;
    }

  g_print("Indexed %d files in %d.%06d seconds\n", ctx.file_id, tv2.tv_sec, tv2.tv_usec);

  return 0;
}

static void
index_dir(IndexContext *ctx, const char *path) /* Go through all the files underneath this directory */
{
  DIR *dh;
  struct dirent *dent;
  char cbuf[PATH_MAX];

  dh = opendir(path);
  if(!dh)
    return;

  readdir(dh); /* Skip . & .. */
  readdir(dh);

  while((dent = readdir(dh)))
    {
      g_snprintf(cbuf, sizeof(cbuf), "%s/%s", path, dent->d_name);
      index_path(ctx, cbuf);
    }
  closedir(dh);
}

static int			/* non-zero return is error */
index_html(IndexContext *ctx, const char *path, BTRECNUM file_id, size_t size)
{
  return 1;
}

static int			/* non-zero return is error */
index_txt(IndexContext *ctx, const char *path, BTRECNUM file_id, size_t size)
{
  int fd;
  char *text, *pos, *last_word, *end;

  fd = open(path, O_RDONLY);
  if(fd < 0)
    return 1;

  text = mmap(NULL, size+1, PROT_READ, MAP_SHARED, fd, 0);
  if(text == MAP_FAILED)
    {
      close(fd);
      return 1;
    }

  for(last_word = NULL, pos = text, end = text + size; pos < end; pos++)
    {
      if(isspace(*pos))
	{
	  if(last_word)
	    {
	      char cbuf[129];
	      int nbytes = MIN(pos - last_word, sizeof(cbuf)-1);
	      strncpy(cbuf, last_word, nbytes);
	      cbuf[nbytes] = '\0';
	      word_add(ctx, cbuf, file_id);
	      last_word = NULL;
	    }

	  continue;
	}

      if(!last_word && isalnum(*pos))
	last_word = pos;
    }

  munmap(text, size+1);
  close(fd);

  return 0;
}

static void
index_path(IndexContext *ctx, const char *path)
{
  const char *ext;
  gboolean do_fileadd;
  BTRECNUM my_id;
  struct stat sbuf;

  stat(path, &sbuf);
  if(S_ISDIR(sbuf.st_mode))
    {
      index_dir(ctx, path);
      return;
    }

  /* If not that, try to figure out what type of file it is */

  ext = g_extension_pointer(path);

  my_id = ctx->file_id;

  if(!strcasecmp(ext, "htm")
     || !strcasecmp(ext, "html"))
    do_fileadd = !index_html(ctx, path, my_id, sbuf.st_size);
  else if(!strcasecmp(ext, "txt"))
    do_fileadd = !index_txt(ctx, path, my_id, sbuf.st_size);
  else
    do_fileadd = FALSE;

  if(do_fileadd)
    {
      g_print("%s\n", path);
      filename_add(ctx, path, my_id);
      ctx->file_id++;
    }
}

static void
filename_add(IndexContext *ctx, const char *path, BTRECNUM file_id)
{
  btinsert(ctx->bt_fn, path, file_id);
  riwrite(ctx->rf_fn, file_id, path, strlen(path), FALSE);
}

static void
word_add(IndexContext *ctx, const char *word, BTRECNUM file_id)
{
  BTRECNUM file_check[NFILE_CHECK];
  int r, nread;
  RIREC word_id;
  BTNODENUM node_num;
  char *key;
  int i, nitems;

  switch(btfind(ctx->bt_w2f, word, &node_num, &key, &word_id))
    {
    case BT_NREC:
      word_id = ctx->word_id++;
      r = btinsert(ctx->bt_w2f, word, word_id);
      g_assert(r != BT_ERR);
      break;
    case BT_ERR:
      g_error("Got BT_ERR from btfind");
      break;
    default:
      break;
    }

  for(r = nread = 0; nread < sizeof(file_check); nread += r) {

    r = riread(ctx->rf_w2f, word_id, file_check + nread, sizeof(file_check) - nread, (nread>0)?TRUE:FALSE);
    if(r <= 0) break;
    if(r < (sizeof(file_check) - nread))
      {
	nread += r;
	break;
      }
  };

  nitems = nread/sizeof(file_check[0]);
  if(nitems)
    for(i = 0; i < nitems; i++)
      {
	/* Already listed */
	if(file_check[i] == file_id)
	  return;
      }

  file_check[nitems] = file_id;

  riwrite(ctx->rf_w2f, word_id, file_check, (nitems+1)*sizeof(file_check[0]), FALSE);
}
